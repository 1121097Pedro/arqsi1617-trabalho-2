<?php

// Pull in the NuSOAP code
require_once('nusoap.php');
// Create the server instance
$server = new soap_server();
// Initialize WSDL support
$server->configureWSDL('service', 'urn:service?wsdl');

$server->wsdl->addComplexType(
        'saleDetails', 'complexType', 'array', 'all', '', array(
    "Artist" => array("name" => "Artist", "type" => "xsd:string"),
    'Title' => array("name" => 'Title', "type" => 'xsd:string'),
    'Format' => array("name" => 'Format', 'type' => 'xsd:string'),
    'Quantity' => array("name" => 'Quantity', 'type' => 'xsd:int')
        )
);

$server->register('registerSale',                // method name
    array("sale" => "tns:saleDetails"),        // input parameters
    array('return' => 'xsd:int'),      // output parameters
    'urn:service',                      // namespace
    'urn:service#registerSale',                // soapaction
    'rpc',                                // style  
    'encoded',                            // use
    'Register a sale'            // documentation
);

function registerSale($saleDetails) {
    $conn = new mysqli("localhost", "i121097", "9114919", "i121097");
    if ($conn->connect_errno > 0) {
        die('Unable to connect to database [' . $conn->connect_error . ']');
    }
    print_r($saleDetails);
    
    $createSaleSQL = "INSERT INTO `ImportMusic` (`ArtistName`, `AlbumTitle`,`Format`,`Quantity`) VALUES (" .
            "'" . $saleDetails['Artist'] . "'," .
            "'" . $saleDetails['Title'] . "','" . $saleDetails['Format'] . "', " . $saleDetails["Quantity"]. ")";
    $result = $conn->query($createSaleSQL);
    return $result;
}

// Use the request to (try to) invoke the service
$HTTP_RAW_POST_DATA = isset($HTTP_RAW_POST_DATA) ? $HTTP_RAW_POST_DATA : '';
$server->service($HTTP_RAW_POST_DATA);
?>
