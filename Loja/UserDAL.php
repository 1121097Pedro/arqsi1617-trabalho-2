<?php

include "dbConfig.php";

function CreateUserDAL() {
    global $servername;
    global $username;
    global $pass;
    global $dbname;
    return new UserDAL($servername, $username, $pass, $dbname);
}
class UserDAL {

    public $conn;

    public function __construct($servername, $username, $pass, $dbname) {
        $this->conn = new mysqli($servername, $username, $pass, $dbname);
        if ($this->conn->connect_errno > 0) {
            die('Unable to connect to database [' . $this->conn->connect_error . ']');
        }
    }
    
    public function userExists($username){
        $sqlQuery = "SELECT * FROM `User` WHERE `LoginName` = '$username'";
        $result = $this->conn->query($sqlQuery);
        $row = $result->num_rows;
        return $row;
    }
    
    public function getUserID($username) {
        $sqlQuery = "SELECT * FROM `User` where `LoginName` = '$username'";
        $result = $this->conn->query($sqlQuery);
        return $result->fetch_object()->UserID;
    }
    
    public function tryLogin($username, $password){
        $sqlQuery = "SELECT * FROM `User` where `LoginName` = '$username' and `Password` = '$password'";
        $result = $this->conn->query($sqlQuery);
        $row = $result->num_rows;
        return $row;
    }
    
    public function registerUser($username, $password, $address){
        $sqlQuery = "INSERT INTO `User` (`LoginName`, `Password`, `Address`) VALUES('$username', '$password', '$address')";
        $result = $this->conn->query($sqlQuery);
        return $result;
    }
    
    public function isAdmin($username){
        $sqlQuery = "SELECT `Admin` FROM `User` where `LoginName` = '$username'";
        $result = $this->conn->query($sqlQuery);
        $adminValue = $result->fetch_object()->Admin;
        return $adminValue;
    }
    
    public function close() {
        $this->conn->close();
    }
    
}

?>