<?php
session_start();
?>

<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=ISO 8859-1">
        <title>Music Shop</title>
        <link rel="stylesheet" type="text/css" href="css/admin.css"/>
        <script src="//ajax.googleapis.com/ajax/libs/jquery/1.8.2/jquery.min.js"></script>
        <link href="css/pag_principal.css" rel="stylesheet" type="text/css" media="all"/>
        <link href="css/catalog.css" rel="stylesheet" type="text/css" />
    </head>
    <body>
        <?php
        include 'header.php';
        ?>
        <div class="middle" id="middleDiv">
            <table id="recordsTable">
                <tr>	
                    <th>Artist</th>
                    <th>Album</th>
                    <th>Format</th>
                    <th>Price</th>
                    <th>Reference ID</th>
                    <th>Tags</th>
                </tr>
            </table>

            <h2>Cart</h2>
            <div id="cartDisplayDiv">

            </div>
        </div>



        <script type="text/javascript">
            var records;

            function loadCart() {
                var url = "AddToCart.php";
                $.ajax({
                    url: url,
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: refreshCart,
                    error: errorFunc
                });
            }

            function addToCart(id) {
                var url = "AddToCart.php?id=" + id;
                $.ajax({
                    url: url,
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: refreshCart,
                    error: errorFunc
                });

            }

            function removeFromCart(id) {
                var url = "RemoveFromCart.php?id=" + id;
                $.ajax({
                    url: url,
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: refreshCart,
                    error: errorFunc
                });
            }

            function refreshCart(data, status) {
                var count = 0;
                $("#cartDisplayDiv").empty();
                $.each(data, function (index, val) {
                    count++;
                    if (records.hasOwnProperty(index)) {
                        var cartItem = createCartItem(records, index, val);

                        $("#cartDisplayDiv").append(cartItem);
                    }
                });
                if (count > 0) {
                    var makeOrderLink = document.createElement("a");
                    makeOrderLink.href = "MakeOrder.php";
                    makeOrderLink.setAttribute("class", "button");
                    makeOrderLink.appendChild(document.createTextNode("Make Order"));
                    $("#cartDisplayDiv").append(makeOrderLink);
                }
            }

            function createCartItem(records, index, val) {
                var cartElement = document.createElement("li");
                cartElement.setAttribute("class", "albumItem");
                var header = document.createElement("h3");
                header.appendChild(document.createTextNode(records[index].Title));
                cartElement.appendChild(header);
                var p = document.createElement("p");
                p.appendChild(document.createTextNode("Artist: " + records[index].Artist));
                cartElement.appendChild(p);
                p = document.createElement("p");
                p.appendChild(document.createTextNode(" Title: " + records[index].Title));
                cartElement.appendChild(p);
                p = document.createElement("p");
                p.appendChild(document.createTextNode(" Format: " + records[index].Format));
                cartElement.appendChild(p);
                p = document.createElement("p");
                p.appendChild(document.createTextNode(" Price: " + records[index].Price));
                cartElement.appendChild(p);
                p = document.createElement("p");
                p.appendChild(document.createTextNode(" Quantity: " + val));
                cartElement.appendChild(p);
                var buttonElement = document.createElement("button");
                buttonElement.setAttribute("type", "button");
                buttonElement.setAttribute("id", "removeButton");
                buttonElement.setAttribute("onClick", "removeFromCart(" + index + ");");
                buttonElement.appendChild(document.createTextNode("Remove From Cart"));
                cartElement.appendChild(buttonElement);
                return cartElement;
            }

            function errorFunc(data, status) {

            }

            function createRecordsTable(data, status) {
                var count = 0;
                records = data;
                $.each(data, function (index, array) {
                    var tr = document.createElement("tr");
                    if (count % 2 == 0)
                        tr.setAttribute("class", "evenTR");
                    else
                        tr.setAttribute("class", "oddTR");
                    var td = document.createElement("td");
                    td.appendChild(document.createTextNode((array["Artist"])));
                    tr.appendChild(td);
                    td = document.createElement("td");
                    td.appendChild(document.createTextNode((array["Title"])));
                    tr.appendChild(td);
                    td = document.createElement("td");
                    td.appendChild(document.createTextNode((array["Format"])));
                    tr.appendChild(td);
                    td = document.createElement("td");
                    td.appendChild(document.createTextNode((array["Price"])));
                    tr.appendChild(td);
                    td = document.createElement("td");
                    td.appendChild(document.createTextNode((index)));
                    tr.appendChild(td);
                    td = document.createElement("td");
                    $.each(array["Tags"], function (index, val) {
                        td.appendChild(document.createTextNode(val));
                    });
                    tr.appendChild(td);
                    td = document.createElement("td");
                    var buttonElement = document.createElement("button");
                    buttonElement.setAttribute("type", "button");
                    buttonElement.setAttribute("onClick", "addToCart(" + index + ");");
                    buttonElement.appendChild(document.createTextNode("Add to Cart"));
                    td.appendChild(buttonElement);
                    tr.appendChild(td);

                    $("#recordsTable").append(tr);
                    count++;
                });
                loadCart();
            }

            function loadRecords() {
                var url = "GetCatalogJSON.php";
                $.ajax({
                    url: url,
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: createRecordsTable,
                    error: errorFunc
                });
            }

            loadRecords();
        </script>
    </body>
</html>
