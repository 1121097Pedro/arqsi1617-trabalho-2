<?php
include "UserDAL.php";

$error = 0;

if (isset($_POST["nome"]) &&
        isset($_POST["address"]) &&
        isset($_POST["password"]) &&
        isset($_POST["password2"])) {
    $userDAL = CreateUserDAL();

    $nome = mysqli_real_escape_string($userDAL->conn, $_POST["nome"]);
    $address = mysqli_real_escape_string($userDAL->conn, $_POST["address"]);
    $password = mysqli_real_escape_string($userDAL->conn, $_POST["password"]);
    $password2 = mysqli_real_escape_string($userDAL->conn, $_POST["password2"]);

    if ($userDAL->userExists($nome) != 0) { // username j� existe no sistema
        $error = 2;
    }
    if ($password != $password2) { //Passwords n�o coincidem
        $error = 1;
    }

    if ($error == 0) { // Sem errors, registar
        $result = $userDAL->registerUser($nome, $password, $address);
        if ($result == 1) {
            session_start();
            $_SESSION["username"] = $nome;
            header("Location: Home.php");
            die();
        }
    }

    $userDAL->close();
}
?>

<!DOCTYPE html>
<html>
    <head>
        <title>Register - Music Shop</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link href="css/default.css" rel="stylesheet" type="text/css" media="all"/>
        <script src="js/registar.js"></script>
    </head>
    <body>
        <div class="contentor">
            <h3>Register</h3>

            <div>
                <form method="POST" action="registar.php" onSubmit="return validarFormulario();" >
                    <label id="mensagem_erro">
                        <?php
                        if ($error == 1) {
                            echo "Passwords do not match";
                        }
                        if ($error == 2) {
                            echo "Username not available";
                        }
                        ?></label>
                    <table class="dados" width="400">
                        <tr><td>My Name</td><td><input type="text" id="nome" name="nome" ></td></tr>
                        <tr><td>My Address</td><td><input type="text" id="address" name="address"></td></tr>
                        <tr><td>My Password</td><td><input type="password" id="password" name="password"></td></tr>
                        <tr><td>Confirm Password</td><td><input type="password" id="password2" name="password2"></td><td></tr> 
                    </table> 
                    <p><input type="submit" value="Register"></p>
                </form>
            </div>    
        </div>

    </body>
</html>
