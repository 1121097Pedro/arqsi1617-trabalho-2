<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

include "UserDAL.php";
$userDAL = CreateUserDAL();

session_start();
if (isset($_POST["loginName"]) && isset($_POST["password"])) {
    $loginName = $_POST["loginName"];
    $password = $_POST["password"];

    $result = $userDAL->tryLogin($loginName, $password);

    if ($result == 0) {
        echo "Login failed";
            $userDAL->close();
    } else {
        $_SESSION["username"] = $loginName;
        if($userDAL->isAdmin($loginName) == 1) {
            $_SESSION["admin"] = 1; 
            header("Location: BrowseEditorCatalog.php");
            die();
        }
        header("Location: Home.php");
        die();
    }
}
?>