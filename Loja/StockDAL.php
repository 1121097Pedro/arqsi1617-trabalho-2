<?php

include "dbConfig.php";

function CreateDAL() {
    global $servername;
    global $username;
    global $pass;
    global $dbname;
    return new StockDAL($servername, $username, $pass, $dbname);
}

class StockDAL {

    public $conn;

    public function __construct($servername, $username, $pass, $dbname) {
        $this->conn = new mysqli($servername, $username, $pass, $dbname);
        if ($this->conn->connect_errno > 0) {
            die('Unable to connect to database [' . $this->conn->connect_error . ']');
        }
    }

    public function addOrder($orderInfo, $records) {
        $this->updateStockWithOrder($orderInfo, $records);
        $this->createOrder($orderInfo, $records);
    }
    
    public function createOrder($orderInfo, $records){
        $createOrderSQL = "INSERT INTO `Order` (`OrderDate`) VALUES (Now())";
        $this->conn->query($createOrderSQL);
        $orderID = $this->conn->insert_id;
        foreach ($orderInfo as $RecordID => $quantity) {
            $record = $records[$RecordID];
            $albumID = $this->getAlbumID($record);
            $sqlQuery = "INSERT INTO `OrderDetail` (`OrderID`,`AlbumID`,`Quantity`) VALUES ($orderID, $albumID, $quantity)";
            $this->conn->query($sqlQuery);
        }
    }

    public function updateStockWithOrder($orderInfo, $records) {
        foreach ($orderInfo as $RecordID => $quantity) {
            $record = $records[$RecordID];
            $this->addAlbumOrUpdate($record, $quantity);
        }
    }

    public function addAlbumOrUpdate($record, $quantity) {
        $exists = $this->albumExists($record["Artist"], $record["Title"], $record["Format"]);
        if ($exists == 0) {
            //Adicionar caso n�o exista
            $this->addAlbum($record, $quantity);
        } else {
            //Incrementar stock caso exista
            $albumID = $this->getAlbumID($record);
            $this->updateStock($albumID, $quantity);
        }
    }

    public function addAlbum($record, $quantity) {
        $artistName = $record["Artist"];
        $albumTitle = $record["Title"];
        $price = $record["Price"];
        $format = $record["Format"];
        $sql = "INSERT INTO `Album` (`ArtistName`, `AlbumTitle`, `Format`, `Price`, `Stock`) VALUES('$artistName', '$albumTitle', '$format', $price, $quantity)";
        $this->conn->query($sql);
        $albumID = $this->getAlbumID($record);
        $this->addTagsToAlbum($albumID, $record["Tags"]);
    }

    public function addTagsToAlbum($albumID, $tags) {
        $tagArray = (array) $tags["string"];
        foreach ($tagArray as $tag) {
            $tagID = $this->getTagID($tag);
            $sqlQuery = "SELECT * FROM `Album_Tag` WHERE `AlbumID` = $albumID and `TagID` = $tagID";
            $result = $this->conn->query($sqlQuery);
            $row = $result->num_rows;
            if ($row == 0) {
                $sqlQuery = "INSERT INTO `Album_Tag` (`AlbumID`, `TagID`) VALUES( $albumID, $tagID)";
                $this->conn->query($sqlQuery);
            }
        }
    }
    
    public function getAlbumTags($albumID){
        $tags = array();
        $sqlQuery = "SELECT * FROM `Album_Tag` WHERE `AlbumID` = $albumID";
        $result = $this->conn->query($sqlQuery);
        while ($row = $result->fetch_object()) {
            $tagID = $row->TagID;
            $sqlQuery = "SELECT `Text` FROM `Tag` WHERE `TagID` = $tagID";
            $tagText = $this->conn->query($sqlQuery)->fetch_object()->Text;
            array_push($tags, $tagText);
        }
        return $tags;
    }

    public function updateStock($albumID, $quantity) {
        $sql = "UPDATE `Album` SET `Stock` = `Stock` + $quantity WHERE `AlbumID` = $albumID";
        $this->conn->query($sql);
    }

    public function getAlbumID($record) {
        $artistName = $record["Artist"];
        $albumTitle = $record["Title"];
        $format = $record["Format"];
        $sqlQuery = "SELECT `AlbumID` FROM `Album` WHERE `ArtistName` = '$artistName' and `AlbumTitle` = '$albumTitle' and `Format` = '$format'";
        $result = $this->conn->query($sqlQuery);
        $albumID = $result->fetch_object()->AlbumID;
        return $albumID;
    }

    public function getTagID($tagName) {
        $sqlQuery = "SELECT `TagID` FROM `Tag` WHERE `Text` = '$tagName'";
        $result = $this->conn->query($sqlQuery);
        $row = $result->num_rows;
        if ($row == 0) {
            $sqlQuery = "INSERT INTO `Tag` (`Text`) VALUES ( '$tagName')";
            $result = $this->conn->query($sqlQuery);
            $sqlQuery = "SELECT `TagID` FROM `Tag` WHERE `Text` = '$tagName'";
            $result = $this->conn->query($sqlQuery);
        }
        return $result->fetch_object()->TagID;
    }

    public function albumExists($ArtistName, $AlbumTitle, $Format) {
        $sqlQuery = "SELECT * FROM `Album` WHERE `ArtistName` = '$ArtistName' and `AlbumTitle` = '$AlbumTitle' and `Format` = '$Format'";
        $result = $this->conn->query($sqlQuery);
        $row = $result->num_rows;
        return $row;
    }

    public function getShopCatalog() {
        $albums = array();
        $sql = "SELECT * FROM `Album` WHERE `Stock` > 0";
        $results = $this->conn->query($sql);
        while ($row = $results->fetch_object()) {
            $album["AlbumID"] = $row->AlbumID;
            $album["ArtistName"] = $row->ArtistName;
            $album["AlbumTitle"] = $row->AlbumTitle;
            $album["Format"] = $row->Format;
            $album["Price"] = $row->Price;
            $album["Stock"] = $row->Stock;
            $album["Tags"] = $this->getAlbumTags($row->AlbumID);
            $albums[$album["AlbumID"]] = $album;
        }
        
        return $albums;
    }

    public function registerSale($userID, $cart){
        $sqlQuery = "INSERT INTO `Sale` (`UserID`, `SaleDate` ) VALUES ( $userID, Now())";
        $this->conn->query($sqlQuery);
        $saleID = $this->conn->insert_id;
        foreach($cart as $albumID => $quantity){
            $sqlQuery = "INSERT INTO `Sale_Album` (`SaleID`, `AlbumID`, `Quantity` ) VALUES ( $saleID, $albumID, $quantity)";
            $this->conn->query($sqlQuery);
            $this->updateStock($albumID, $quantity*-1); // decrementar stock
        }
        
    }
    
    public function close() {
        $this->conn->close();
    }

}

?>