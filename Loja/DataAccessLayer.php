<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

$ligacao = null;

//LIGAÇAO A BASE DE DADOS
function getDbConnection($host, $username, $password, $db) {
    global $ligacao;
    $ligacao = new mysqli($host, $username, $password, $db);
    if ($ligacao->connect_error) {
        die('ligação sem sucesso! ERRO: ' . $ligacao->connect_error);
    }
    return $ligacao;
}

//Executar Comando SQL
function execCommand($strSQL) {
    $resultado = mysqli_query($GLOBALS ['ligacao'], $strSQL) or die('comando sql nao executado! ' . mysql_error());
    
    return $resultado;
}

// Fechar ligação
function closeConnection($ligacao) {
    $GLOBALS ['ligacao']->close();
}
 
?>