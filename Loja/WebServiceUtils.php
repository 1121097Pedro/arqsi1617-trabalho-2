<?php

include 'WebServiceVars.php';

function GetCatalog() {
    global $URL;
    global $API_KEY;
    $records = array();
    $client = new SoapClient($URL . "?wsdl", array("trace" => 1, "exceptions" => 0));
    $param = array("API_KEY" => $API_KEY);
    $result = $client->GetCatalog($param);
    $catalogo = (array) $result->GetCatalogResult;
    if (isset($catalogo['CatalogItem'])) {
        $catalogo = (array) $catalogo['CatalogItem'];
        foreach ($catalogo as $item) {
            $album = (array) $item;
            $records[$album["RecordID"]] = array(
                "Artist" => $album["Artist"],
                "Title" => $album["Title"],
                "Format" => $album["Format"],
                "Price" => $album["Price"],
                "Tags" => (array) $album["Tags"]
            );
        }
    }
    return $records;
}

function MakeOrder($cart) {
    global $URL;
    global $API_KEY;
    $client = new SoapClient($URL . "?wsdl", array("trace" => 1, "exceptions" => 0));
    $order = array();
    foreach ($cart as $id => $qty) {
        array_push($order, "$id,$qty");
    }
    $params = array("API_KEY" => "$API_KEY", "OrderDetails" => $order);
    $result = $client->MakeOrder($params);
}

function RegisterSale($saleInfo) {
    global $URL;
    global $API_KEY;
    /* IDEIMUSIC SECTION */
    $client = new SoapClient($URL . "?wsdl", array("trace" => 1, "exceptions" => 0));
    $saleDetails = array();
    if (isset($saleInfo["Artist"]) && isset($saleInfo["Title"]) && isset($saleInfo["Format"]) && isset($saleInfo["Quantity"])) {
        array_push($saleDetails, $saleInfo["Artist"]);
        array_push($saleDetails, $saleInfo["Title"]);
        array_push($saleDetails, $saleInfo["Format"]);
        array_push($saleDetails, $saleInfo["Quantity"]);
        $saleString = implode(';', $saleDetails);
        $params = array("API_KEY" => "$API_KEY", "SaleDetails" => $saleString);
        $result = $client->RegisterSale($params);
    }

    /* Import Music Section */
    global $URL_IMPORT;
    $client = new SoapClient($URL_IMPORT, array("trace" => 1, "exceptions" => 0));
    if (isset($saleInfo["Artist"]) && isset($saleInfo["Title"]) && isset($saleInfo["Format"]) && isset($saleInfo["Quantity"])) {
        $obj = (object) $saleInfo;
        $client->RegisterSale($obj);
    }
}

?>