<?php
session_start();
if (isset($_SESSION["username"])) {
    ?>
    <!DOCTYPE html>
    <html>
        <head>
            <meta http-equiv="Content-Type" content="text/html; charset=ISO 8859-1">
            <title>Music Shop</title>
            <link href="css/pag_principal.css" rel="stylesheet" type="text/css" media="all"/>
            <link href="css/catalog.css" rel="stylesheet" type="text/css" />
        </head>
        <body>
            <?php
            include 'header.php';
            ?>
            <div class="middle" id="middleDiv">
                <div class="divCatalogo" id="divCatalogo">
                </div> 
                <div id="widgetDiv" class="column-left">
            </div>   
            </div>



        <?php
        include 'footer.php';
        ?>
        <script type="text/javascript">
            var catalog;

            function addToCart(id) {
                var url = "AddToCart.php?id=" + id;
                $.ajax({
                    url: url,
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: function () {
                    },
                    error: errorFunc
                });

            }

            function getCatalog() {
                var url = "GetShopCatalog.php";
                $.ajax({
                    url: url,
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: displayCatalog,
                    error: errorFunc
                });
            }
            function displayCatalog(data, status) {
                catalog = data;
                var list = document.createElement("ul");
                list.setAttribute("class", "albumList");
                $.each(data, function (index, array) {
                    var albumElement = createAlbumElement(array);
                    list.appendChild(albumElement);
                }
                );
                $("#divCatalogo").append(list);
            }

            function createAlbumElement(album) {
                var albumElement = document.createElement("li");
                albumElement.setAttribute("class", "albumItem");
                var header = document.createElement("h3");
                header.appendChild(document.createTextNode(album["AlbumTitle"]));
                albumElement.appendChild(header);
                var artistName = document.createElement("p");
                artistName.appendChild(document.createTextNode("Artist: " + album["ArtistName"]));
                albumElement.appendChild(artistName);

                var artistName = document.createElement("p");
                artistName.appendChild(document.createTextNode("Artist: " + album["ArtistName"]));
                albumElement.appendChild(artistName);

                var format = document.createElement("p");
                format.appendChild(document.createTextNode("Format: " + album["Format"]));
                albumElement.appendChild(format);

                var price = document.createElement("p");
                price.appendChild(document.createTextNode("Price: " + album["Price"] + " �"));
                albumElement.appendChild(price);

                var tags = document.createElement("p");
                tags.appendChild(document.createTextNode("Related Tags: "));
                $.each(album["Tags"], function (index, tag) {
                    var tagLink = document.createElement("a");
                    tagLink.href = "searchByTags.php?tag=" + tag;
                    tagLink.setAttribute("class", "tagLink");
                    tagLink.appendChild(document.createTextNode(tag));
                    tags.appendChild(tagLink);
                });

                var button = document.createElement("button");
                button.setAttribute("type", "button");
                button.setAttribute("onClick", "addToCart(" + album["AlbumID"] + ");");
                button.appendChild(document.createTextNode("Add to Cart"));
                albumElement.appendChild(button);

                albumElement.appendChild(tags);


                return albumElement;
            }

            function errorFunc(data, status) {

            }

            getCatalog();
        </script>
    </body>
    </html>
    <?php
} else {
    header("Location: index.html");
    die();
}
?>