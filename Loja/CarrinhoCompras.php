<?php
session_start();
if (isset($_SESSION["username"])) {
    $cart = array();
    if (isset($_SESSION["cart"])) {
        $cart = $_SESSION["cart"];
    }
    ?>

    <!DOCTYPE html>
    <html>
        <head>
            <meta http-equiv="Content-Type" content="text/html; charset=ISO 8859-1">
            <title>Music Shop</title>
            <link href="css/pag_principal.css" rel="stylesheet" type="text/css" media="all"/>
            <link href="css/catalog.css" rel="stylesheet" type="text/css" />
        </head>
        <body>
            <?php
            include 'header.php';
            ?>
            <div class="middle" id="middleDiv">
                <div id="divCarrinho">
                </div>        
                <div id="widgetDiv" class="column-left">
                </div>   
            </div>
        </div>


        <?php
        include 'footer.php';
        ?>
        <script type="text/javascript">
            var catalog;

            function getCatalog() {
                var url = "GetShopCatalog.php";
                $.ajax({
                    url: url,
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: function (data, status) {
                        catalog = data;
                        loadCart();
                    },
                    error: errorFunc
                });
            }

            function loadCart() {
                var url = "AddToCart.php";
                $.ajax({
                    url: url,
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: refreshCart,
                    error: errorFunc
                });
            }

            function removeFromCart(id) {
                var url = "RemoveFromCart.php?id=" + id;
                $.ajax({
                    url: url,
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: refreshCart,
                    error: errorFunc
                });
            }

            function refreshCart(data, status) {
                var count = 0;
                $("#divCarrinho").empty();
                $.each(data, function (id, qty) {
                    var album = null;
                    if (catalog[id] != undefined) {
                        album = catalog[id];
                    }
                    if (album != null) {
                        var albumElement = createAlbumElement(id, qty, album);
                        count++;
                        $("#divCarrinho").append(albumElement);
                    }
                });
                if (count > 0) {
                    var checkoutLink = document.createElement("a");
                    checkoutLink.href = "Checkout.php";
                    checkoutLink.setAttribute("class", "button");
                    checkoutLink.appendChild(document.createTextNode("Check Out"));
                    $("#divCarrinho").append(checkoutLink);
                }
                else {
                    $("#divCarrinho").append(document.createTextNode("Your cart is empty. "));
                    var link = document.createElement("a");
                    link.href = "Home.php";
                    link.appendChild(document.createTextNode("Browse our catalog !"));
                    $("#divCarrinho").append(link);
                }
            }

            function createAlbumElement(id, qty, album) {
                var albumElement = document.createElement("li");
                albumElement.setAttribute("class", "albumItem");
                var header = document.createElement("h3");
                header.appendChild(document.createTextNode(album["AlbumTitle"]));
                albumElement.appendChild(header);
                var artistName = document.createElement("p");
                artistName.appendChild(document.createTextNode("Artist: " + album["ArtistName"]));
                albumElement.appendChild(artistName);

                var artistName = document.createElement("p");
                artistName.appendChild(document.createTextNode("Artist: " + album["ArtistName"]));
                albumElement.appendChild(artistName);

                var format = document.createElement("p");
                format.appendChild(document.createTextNode("Format: " + album["Format"]));
                albumElement.appendChild(format);

                var price = document.createElement("p");
                price.appendChild(document.createTextNode("Price: " + album["Price"] + " �"));
                albumElement.appendChild(price);
                
                var quantity = document.createElement("p");
                quantity.appendChild(document.createTextNode("Quantity: " + qty));
                albumElement.appendChild(quantity);

                var tags = document.createElement("p");
                tags.appendChild(document.createTextNode("Related Tags: "));
                $.each(album["Tags"], function (index, tag) {
                    var tagLink = document.createElement("a");
                    tagLink.href = "searchByTags.php?tag=" + tag;
                    tagLink.setAttribute("class", "tagLink");
                    tagLink.appendChild(document.createTextNode(tag));
                    tags.appendChild(tagLink);
                });

                var buttonElement = document.createElement("button");
                buttonElement.setAttribute("type", "button");
                buttonElement.setAttribute("id", "removeButton");
                buttonElement.setAttribute("onClick", "removeFromCart(" + id + ");");
                buttonElement.appendChild(document.createTextNode("Remove From Cart"));
                albumElement.appendChild(buttonElement);

                albumElement.appendChild(tags);


                return albumElement;
            }

            function errorFunc(data, status) {

            }

            getCatalog();
        </script>

        <?php
    } else {
        header("Location: index.html");
        die();
    }
    ?>