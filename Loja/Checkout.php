<?php
include 'WebServiceUtils.php';
include 'StockDAL.php';
include 'UserDAL.php';

session_start();

if (!isset($_SESSION["username"])) {
    header("Location: index.html");
    die();
}

$stockDAL = CreateDAL();
$userDAL = CreateUserDAL();
$stock = $stockDAL->getShopCatalog();


$cart = array();
if (isset($_SESSION["cart"])) {
    $cart = (array) $_SESSION["cart"];
}
if (isset($_POST["confirm"])) {
    if (!$userDAL->userExists($_SESSION["username"])) {
        header("Location: index.html");
        die();
    }

    $userID = $userDAL->getUserID($_SESSION["username"]);
    foreach ($cart as $cartItem => $quantity) {
        if (!isset($stock[$cartItem]['ArtistName']) &&
                !isset($stock[$cartItem]['AlbumTitle']) &&
                !isset($stock[$cartItem]['Format']) &&
                !isset($stock[$cartItem]['Price']) &&
                $quantity <= 0) {
            unset($_SESSION[$cartItem]);
            unset($cart[$cartItem]);
        }
    }
    $stockDAL->registerSale($userID, $cart);
    foreach ($cart as $cartItem => $quantity) {
        $album = array(
            "Artist" => $stock[$cartItem]['ArtistName'],
            "Title" => $stock[$cartItem]['AlbumTitle'],
            "Format" => $stock[$cartItem]['Format'],
            "Quantity" => $quantity
        );
        RegisterSale($album);
    }
    $_SESSION["cart"] = array();
    header("Location: Home.php");
    die();
} else {
    ?>
    <form method="POST">
        Do you want to make the following purchase:
        <table>
            <tr>
                <th>Artist</th>
                <th>Album</th>
                <th>Format</th>
                <th>Price</th>
                <th>Quantity</th>
            </tr>
            <?php
            $total = 0;
            foreach ($cart as $albumID => $qt) {
                if (isset($stock[$albumID])) {
                    echo "<tr>";
                    echo "<td>" . $stock[$albumID]['ArtistName'] . "</td>";
                    echo "<td>" . $stock[$albumID]['AlbumTitle'] . "</td>";
                    echo "<td>" . $stock[$albumID]['Format'] . "</td>";
                    echo "<td>" . $stock[$albumID]['Price'] . " �</td>";
                    echo "<td>" . $qt . "</td>";
                    echo "</tr>";
                    $total += $qt * $stock[$albumID]['Price'];
                }
            }
            ?>
        </table>
        <?php
        echo "Total: $total � <br/>";
        ?>
        <input type="hidden" name="confirm">
        <input type='submit' value="Yes" />
        <button type="button" onClick="javascript:location.href = 'CarrinhoCompras.php'">No</input>
    </form> 
    <?php
}?>