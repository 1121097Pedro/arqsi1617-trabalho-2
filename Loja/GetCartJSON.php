<?php

$cart;
session_start();
if (isset($_SESSION["cart"])) {
    $cart = (array) $_SESSION["cart"];
} else {
    $cart = array();
    $_SESSION["cart"] = $cart;
}
echo json_encode($cart);
?>