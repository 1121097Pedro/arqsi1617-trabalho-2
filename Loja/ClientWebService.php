<?php
session_start();
?>

<html>
    <head>
        <link rel="stylesheet" type="text/css" href="admin.css"/>
        <script src="//ajax.googleapis.com/ajax/libs/jquery/1.8.2/jquery.min.js"></script>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    </head>
    <body>
        <table id="recordsTable">
            <tr>	
                <th>Artist</th>
                <th>Album</th>
                <th>Format</th>
                <th>Price</th>
                <th>Reference ID</th>
                <th>Tags</th>
            </tr>
        </table>

        <h2>Cart</h2>
        <div id="cartDisplayDiv">

        </div>


        <script type="text/javascript">
            var records;

            function loadCart() {
                var url = "AddToCart.php";
                $.ajax({
                    url: url,
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: refreshCart,
                    error: errorFunc
                });
            }

            function addToCart(id) {
                var url = "AddToCart.php?id=" + id;
                $.ajax({
                    url: url,
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: refreshCart,
                    error: errorFunc
                });
                
            }
            
            function removeFromCart(id){
               var url = "RemoveFromCart.php?id=" + id;
                $.ajax({
                    url: url,
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: refreshCart,
                    error: errorFunc
                });
            }

            function refreshCart(data, status) {
                var count = 0;
                $("#cartDisplayDiv").empty();
                $.each(data, function (index, val) {
                    count++;
                    var innerDiv = document.createElement("div");
                    var outerDiv = document.createElement("div");
                    innerDiv.setAttribute("class", "cartElement");
                    outerDiv.setAttribute("class", "cartOuterDiv");
                    if (records.hasOwnProperty(index)) {
                        var p = document.createElement("p");
                        p.appendChild(document.createTextNode("Artist: " + records[index].Artist));
                        innerDiv.appendChild(p);
                        p = document.createElement("p");
                        p.appendChild(document.createTextNode(" Title: " + records[index].Title));
                        innerDiv.appendChild(p);
                        p = document.createElement("p");
                        p.appendChild(document.createTextNode(" Format: " + records[index].Format));
                        innerDiv.appendChild(p);
                        p = document.createElement("p");
                        p.appendChild(document.createTextNode(" Price: " + records[index].Price));
                        innerDiv.appendChild(p);
                        p = document.createElement("p");
                        p.appendChild(document.createTextNode(" Quantity: " + val));
                        innerDiv.appendChild(p);
                        var buttonElement = document.createElement("button");
                        buttonElement.setAttribute("type", "button");
                        buttonElement.setAttribute("id", "removeButton");
                        buttonElement.setAttribute("onClick", "removeFromCart(" + index + ");");
                        buttonElement.appendChild(document.createTextNode("Remove From Cart"));
                        outerDiv.appendChild(innerDiv);
                        outerDiv.appendChild(buttonElement);
                        $("#cartDisplayDiv").append(outerDiv);
                    }
                });
                if(count > 0){
                    var makeOrderLink = document.createElement("a");
                    makeOrderLink.href = "MakeOrder.php";
                    makeOrderLink.appendChild(document.createTextNode("Make Order"));
                    $("#cartDisplayDiv").append(makeOrderLink);
                }
            }

            function errorFunc(data, status) {

            }

            function createRecordsTable(data, status) {
                records = data;
                $.each(data, function (index, array) {
                    var tr = document.createElement("tr");
                    var td = document.createElement("td");
                    td.appendChild(document.createTextNode((array["Artist"])));
                    tr.appendChild(td);
                    td = document.createElement("td");
                    td.appendChild(document.createTextNode((array["Title"])));
                    tr.appendChild(td);
                    td = document.createElement("td");
                    td.appendChild(document.createTextNode((array["Format"])));
                    tr.appendChild(td);
                    td = document.createElement("td");
                    td.appendChild(document.createTextNode((array["Price"])));
                    tr.appendChild(td);
                    td = document.createElement("td");
                    td.appendChild(document.createTextNode((index)));
                    tr.appendChild(td);
                    td = document.createElement("td");
                    $.each(array["Tags"], function (index, val) {
                        td.appendChild(document.createTextNode(val));
                    });
                    tr.appendChild(td);
                    td = document.createElement("td");
                    var buttonElement = document.createElement("button");
                    buttonElement.setAttribute("type", "button");
                    buttonElement.setAttribute("onClick", "addToCart(" + index + ");");
                    buttonElement.appendChild(document.createTextNode("Add to Cart"));
                    td.appendChild(buttonElement);
                    tr.appendChild(td);

                    $("#recordsTable").append(tr);
                });
                loadCart();
            }

            function loadRecords() {
                var url = "GetCatalogJSON.php";
                $.ajax({
                    url: url,
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: createRecordsTable,
                    error: errorFunc
                });
            }

            loadRecords();
        </script>
    </body>
</html>
