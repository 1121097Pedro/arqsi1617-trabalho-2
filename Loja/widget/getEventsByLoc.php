<?php

###TODO: os resultados estão automaticamente paginados do lado do lastFM, é necessario acautelar para quando queremos páginas posteriores, isto é, do lado do cliente se 'esgotaram' resultados a mostrar

include 'lastFM.php';
include "updateDB.php";

function getGeo($venueElement){
	$loc = $venueElement->item(0)->getElementsByTagName("location")->item(0);
	$geopoint = $loc->getElementsByTagName('point')->item(0);
	$latitude = $geopoint->getElementsByTagName("lat")->item(0)->nodeValue;
	$longitude = $geopoint->getElementsByTagName("long")->item(0)->nodeValue;
	
	return Array('latitude' => $latitude, "longitude" => $longitude);
}

 if(isset($_GET['location']) &&  isset($_GET['distance'])) {
	$loc = $_GET['location'];
	$dist = $_GET['distance'];
	if(isset($_GET['page'])){
		$page = $_GET['page'];
	}
	else {
		$page = 1;
	}
	$req_str = "http://ws.audioscrobbler.com/2.0/?method=geo.getevents";
	$req_str .= "&location=" . urlencode($loc);
	$req_str .= "&distance=" . $dist; 
	$req_str .= "&page=" . $page; 
	$req_str .= "&api_key=" . $API_KEY;
	$respostaXML=file_get_contents($req_str);

	$newXML= new DOMDocument('1.0', 'ISO-8859-1');
	$newXML->loadXML($respostaXML);

	$nodelist=$newXML->getElementsByTagName("event");
	$events = Array();
	/*
		Iterar cada evento pretendido
	*/
	foreach($nodelist as $event){
		$arr = Array();
		$arr['title'] = $event->getElementsByTagName("title")->item(0)->nodeValue;
		$artists = $event->getElementsByTagName("artists")->item(0);
		$headliner = $event->getElementsByTagName("headliner")->item(0)->nodeValue;
		$arr['headliner'] = $headliner;
		$arr['startDate'] = $event->getElementsByTagName("startDate")->item(0)->nodeValue;
		$venue = $event->getElementsByTagName("venue");
		$venueName = $venue->item(0)->getElementsByTagName("name")->item(0)->nodeValue;
		
		$coords  = getGeo($venue);
		
		$arr['place'] = $venueName;
		$arr["coords"] = $coords;
		array_push($events,$arr);
	}
	 echo json_encode($events);
	 }
?>