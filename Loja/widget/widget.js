var httpObj;
var API_LOADED = false;
var events = new Array();
var eventsIndex = 0;
var eventsPage = 1;
var eventsLocation;
var eventsDistance;
var map;

// Funções para o mapa

function initialize() {
    /*
    Inicializar mapa googlemaps dentro da div maps-div
    */
    var mapOptions = {
        zoom: 10,
        center: new google.maps.LatLng(-34.397, 150.644)
    };

    map = new google.maps.Map(document.getElementById('maps-div'), mapOptions);
    fillMap();
}

function loadScript() {

    if (API_LOADED == false) {
        /*
            Carregar API googlemaps apenas uma vez
        */
        var script = document.createElement('script');
        script.type = 'text/javascript';
        script.src = 'https://maps.googleapis.com/maps/api/js?v=3.exp&' + 'callback=initialize';
        document.body.appendChild(script);
        API_LOADED = true;
    }
    else {
        initialize();
    }
}

function fillMap() {
    for (var i = 0; i < events.length; i++) {

        /*
       Adição dos marcadores ao mapa !
       */
        var latlng = new google.maps.LatLng(events[i].coords.latitude, events[i].coords.longitude);


        var marker = new google.maps.Marker({
            position: latlng,
            map: map,
            title: events[i].title
        });

        /*
        Recentrar o mapa no ponto mais recente
        */

        map.setCenter(latlng);
    }
}

function showMap() {
    if (!document.getElementById("maps-div")) {
        /*
        Criar div para conter mapa se não existir
        */
        var div = getWidgetDiv();
        var mapsDiv = document.createElement("div");
        mapsDiv.id = "maps-div";
        mapsDiv.setAttribute("style", "height: 300px")
        div.appendChild(mapsDiv);
        loadScript();
    }

}


// Funções utilitárias


function getWidgetDiv() {
    var div = document.getElementById("widgetDiv");
    return div;
}

function CreateXmlHttpRequestObject() {
    // detecção do browser simplificada
    // e sem tratamento de excepções
    httpObj = null;
    if (window.XMLHttpRequest) // IE 7 e Firefox
    {
        httpObj = new XMLHttpRequest();
    }
    else if (window.ActiveXObject) // IE 5 e 6
    {
        httpObj = new ActiveXObject("Microsoft.XMLHTTP");
    }
    return httpObj;
}

function makeXmlHttpRequest(URL, stateHandler) {
    httpObj = CreateXmlHttpRequestObject();

    if (httpObj) {
        // Definição do URL para efectuar pedido HTTP - método GET
        httpObj.open("GET", URL, true);
        httpObj.onreadystatechange = stateHandler;
        httpObj.send(null);
    }
}

function clearElement(element) {
    while (element.firstChild) {
        element.removeChild(element.firstChild);
    }

}

// AJAX functions

function getTagsTrack(tag) {
    /*
        Dada uma tag, obter as top tracks dessa tag
    */
    var num = document.getElementById("numTracks").value;
    if (!isNaN(num) && num != "") { // número válido
        makeXmlHttpRequest("widget/getTracksFromTag.php?tag=" + tag + "&num=" + num, tagInfoStateHandler);
    }

}

function tagInfoStateHandler() {
    if (httpObj.readyState == 4 && httpObj.status == 200) {
        /*
         Obter resposta como JSON e invocar função para criar a tabela das tracks
        */
        var tracks = JSON.parse(httpObj.responseText); // obter resposta como JSON
        createTrackTable(tracks);
    }
}

function createTrackTable(tracks) {
    var div = getWidgetDiv();
    clearElement(div);
    var table = document.createElement("table");
    table.id = "tabela";

    /*
        Criar Headers da tabela
    */
    var header = document.createElement("th");
    var tr = document.createElement("tr");
    header.textContent = "Artist";
    tr.appendChild(header);
    header = document.createElement("th");
    header.textContent = "Track";
    tr.appendChild(header);
    table.appendChild(tr);

    tracks = tracks.toptracks.track;
    for (var i = 0; i < tracks.length; i++) {
        var trackRow = document.createElement("tr");

        var artistName = tracks[i].artist.name;
        var songName = tracks[i].name;
        /*
            Função onclick anonima
        */
        trackRow.onclick = (
            function (a, m) {
                return function () {
                    trackSelected(a, m);
                }
            }(artistName, songName));

        var artistCell = document.createElement("td");
        artistCell.textContent = artistName;


        /*
            Criação do link com tooltip
        */
        var songCell = document.createElement("td");
        var tooltipLink = document.createElement("a");
        tooltipLink.setAttribute("href", "#");
        tooltipLink.setAttribute("class", "tooltip");
        tooltipLink.textContent = songName;
        var tooltipSpan = document.createElement("span");
        tooltipSpan.id = "tooltip" + songName;
        tooltipSpan.setAttribute("style", "display: none");
        tooltipLink.appendChild(tooltipSpan);
        songCell.appendChild(tooltipLink);

        /*
        Cores de fundo a alternar
        */
        if (i % 2 == 0) {
            artistCell.setAttribute("class", "td-even");
            songCell.setAttribute("class", "td-even");
        }
        else {
            artistCell.setAttribute("class", "td-odd");
            songCell.setAttribute("class", "td-odd");
        }

        trackRow.appendChild(artistCell);
        trackRow.appendChild(songCell);
        table.appendChild(trackRow);
    }

    div.appendChild(table);
}

function trackSelected(artist, song) {
    makeXmlHttpRequest("widget/getTrackInfo.php?artist=" + artist + "&track=" + song, getTrackInfo);
}

function getTrackInfo() {
    if (httpObj.readyState == 4 && httpObj.status == 200) {
        var trackInfo = JSON.parse(httpObj.responseText);
        fillToolTip(trackInfo);

    }
}

function fillToolTip(trackInfo) {
    var tooltip = document.getElementsByTagName("span");
    for (var i = 0; i < tooltip.length; i++) {
        if (tooltip[i].id == "tooltip" + trackInfo.trackName) {
            if (tooltip[i].style.display == 'none') {
                tooltip[i].removeAttribute("style");
                var tabeladisplay = document.createElement("table");
                tabeladisplay.className = "tabelaInfo";

                //Nome do artista
                var artistRow = document.createElement("tr");
                var artist1 = document.createElement("td");
                artist1.appendChild(document.createTextNode("Artist"));
                var artist2 = document.createElement("td");
                artist2.appendChild(document.createTextNode(trackInfo.artistName));
                artistRow.appendChild(artist1);
                artistRow.appendChild(artist2);
                tabeladisplay.appendChild(artistRow);

                //Imagem do artist
                var artistImageRow = document.createElement("tr");
                var artistImageCell = document.createElement("td");
                artistImage = document.createElement("img");
                artistImage.src = trackInfo.artistImageURL;
                artistImageCell.appendChild(artistImage);
                artistImageRow.appendChild(document.createElement("td"));
                artistImageRow.appendChild(artistImageCell);
                tabeladisplay.appendChild(artistImageRow);


                // Nome do álbum
                var albumRow = document.createElement("tr");
                var albumLabel = document.createElement("td");
                albumLabel.appendChild(document.createTextNode("Album"));
                var albumName = document.createElement("td");
                albumName.appendChild(document.createTextNode(trackInfo.albumName));
                albumRow.appendChild(albumLabel);
                albumRow.appendChild(albumName);
                tabeladisplay.appendChild(albumRow);

                //Imagem do Album

                if (trackInfo.musicBrainzInfo != null) {
                    var imageURL = trackInfo.musicBrainzInfo.images[0].image;

                    if (imageURL != "Not found") {
                        var albumImageRow = document.createElement("tr");
                        var albumImageCell = document.createElement("td");
                        albumImage = document.createElement("img");
                        albumImage.src = trackInfo.musicBrainzInfo.images[0].image;
                        albumImageCell.appendChild(albumImage);
                        albumImageRow.appendChild(document.createElement("td"));
                        albumImageRow.appendChild(albumImageCell);
                        tabeladisplay.appendChild(albumImageRow);
                    }

                }
                var topAlbumsRow = document.createElement("tr");
                var topAlbumsLabel = document.createElement("td");
                var topAlbums = document.createElement("td");
                topAlbumsLabel.appendChild(document.createTextNode("Top Albums"));

                var list = document.createElement("ol");
                var el1 = document.createElement("li");
                var el1Text = document.createTextNode(trackInfo.artistTopAlbums[0]);
                el1.appendChild(el1Text);
                list.appendChild(el1);
                var el2 = document.createElement("li");
                var el2Text = document.createTextNode(trackInfo.artistTopAlbums[1]);
                el2.appendChild(el2Text);
                list.appendChild(el2);
                var el3 = document.createElement("li");
                var el3Text = document.createTextNode(trackInfo.artistTopAlbums[2]);
                el3.appendChild(el3Text);
                list.appendChild(el3);

                topAlbums.appendChild(list);
                topAlbumsRow.appendChild(topAlbumsLabel);
                topAlbumsRow.appendChild(topAlbums);
                tabeladisplay.appendChild(topAlbumsRow);


                // TOP TRACK

                var topTrackRow = document.createElement("tr");
                var topTrackLabel = document.createElement("td");
                topTrackLabel.appendChild(document.createTextNode("Top Track"));
                var topTrackCell = document.createElement("td");
                topTrackCell.appendChild(document.createTextNode(trackInfo.topTrackName));

                topTrackRow.appendChild(topTrackLabel);
                topTrackRow.appendChild(topTrackCell);
                tabeladisplay.appendChild(topTrackRow);

                clearElement(tooltip[i]);
                tooltip[i].appendChild(tabeladisplay);
            }
            else {
                tooltip[i].setAttribute("style", "display:none");
            }
        }
        else {
            tooltip[i].setAttribute("style", "display:none");
        }
    }

}


function createTagTable(tags) {
    var div = getWidgetDiv(); // obter div onde se vai dispor os resultados
    clearElement(div);
    var textBox = document.createElement("input");
    textBox.id = "numTracks";
    textBox.type = "text";
    textBox.style = "width: 30px";
    textBox.value = 20;
    var textBoxDesc = document.createElement("label");
    textBoxDesc.setAttribute("for", "numTracks");
    textBoxDesc.textContent = "Number of tracks";

    div.appendChild(textBoxDesc);
    div.appendChild(textBox);
    var table = document.createElement("table");
    table.id = "tabela";

    var header = document.createElement("th");
    header.textContent = "Tag Name";
    var tr = document.createElement("tr");
    tr.appendChild(header);
    table.appendChild(tr);

    /*
    Criação das linhas da tabela
    */
    for (var i = 0; i < tags.length; i++) {
        tag = tags[i];
        var tr = document.createElement("tr");
        var td = document.createElement("td");

        td.textContent = tag;

        // Para obter cores de fundo a alternar
        if (i % 2 == 0) {
            td.setAttribute("class", "td-even");
        }
        else {
            td.setAttribute("class", "td-odd");
        }


        /*
        Função anonima para o onclick da linha, invocar getTagsTrack quando se clicka na linha
        */
        tr.onclick = (
            function (t) {
                return function () {
                    getTagsTrack(t);
                }
            }(tag));
        tr.appendChild(td);
        table.appendChild(tr);
    }
    div.appendChild(table);
}

function tagsStateHandler() {

    if (httpObj.readyState == 4 && httpObj.status == 200) {
        var tags = httpObj.responseText.split(",");
        createTagTable(tags);
    }

}



function searchArtistTags() {
    var artistName = document.getElementById("artistBox").value;
    var numTags = document.getElementById("numTagBox").value;
    if (isNaN(numTags)) {
        // Se numTags não for número, retornar
        return;
    }
    var numTags = parseInt(numTags);
    if (artistName != '' && numTags > 0) {
        makeXmlHttpRequest('widget/getArtistTopTags.php?name=' + artistName + "&numTags=" + numTags, tagsStateHandler);
    }

}


function eventsPreviousPage() {
    eventsIndex -= 5;
    if (eventsIndex < 0)
        eventsIndex = 0;
    createEventsTable();
}

function eventsNextPage() {
    eventsIndex += 5;
    createEventsTable();
}



function createEventsTable() {
    var table = document.getElementById("eventsTable");
    /*
    Se a tabela não existir, criar
    */
    if (!table) {
        var div = getWidgetDiv();
        clearElement(div);
        var table = document.createElement("table");
        table.id = "eventsTable";

        div.appendChild(table);
        var prevButton = document.createElement("input");
        prevButton.type = "button";
        prevButton.value = "Previous";
        prevButton.onclick = eventsPreviousPage;
        var nextButton = document.createElement("input");
        nextButton.type = "button";
        nextButton.value = "Next";
        nextButton.onclick = eventsNextPage;
        var showMapButton = document.createElement("input");
        showMapButton.type = "button";
        showMapButton.value = "Show Map";
        showMapButton.onclick = showMap;
        div.appendChild(prevButton);
        div.appendChild(nextButton);
        div.appendChild(showMapButton);
    }

    /*
    Limpar tabela caso não esteja vazia
    */
    clearElement(table);

    var headers = document.createElement("tr");
    var header = document.createElement("th");
    header.textContent = "Title";
    headers.appendChild(header);
    header = document.createElement("th");
    header.textContent = "Location";
    headers.appendChild(header);
    header = document.createElement("th");
    header.textContent = "Head Liner";
    headers.appendChild(header);
    header = document.createElement("th");
    header.textContent = "Start Date";
    headers.appendChild(header);
    table.appendChild(headers);
    for (var i = eventsIndex; i < eventsIndex + 5; i++) {
        if (eventsIndex >= events.length) {
            eventsPage++;
            searchMoreEvents();
            return;
        }
        var tableRow = document.createElement("tr");
        var eventTitleCell = document.createElement("td");
        eventTitleCell.textContent = events[i].title;

        var locationCell = document.createElement("td");
        locationCell.textContent = events[i].place;
        var headlinerCell = document.createElement("td");
        headlinerCell.textContent = events[i].headliner;
        var startDateCell = document.createElement("td");
        startDateCell.textContent = events[i].startDate;
		
		/*
		Cores de fundo a alternar
		*/
		if (i % 2 == 0) {
            eventTitleCell.setAttribute("class", "td-even");
			locationCell.setAttribute("class", "td-even");
			headlinerCell.setAttribute("class", "td-even");
			startDateCell.setAttribute("class", "td-even");
        }
        else {
            eventTitleCell.setAttribute("class", "td-odd");
			locationCell.setAttribute("class", "td-odd");
			headlinerCell.setAttribute("class", "td-odd");
			startDateCell.setAttribute("class", "td-odd");
        }
		
		tableRow.appendChild(eventTitleCell);
		tableRow.appendChild(locationCell);
		tableRow.appendChild(headlinerCell);
		tableRow.appendChild(startDateCell);
        table.appendChild(tableRow);
    }
}

function eventsStateHandler() {


    if (httpObj.readyState == 4 && httpObj.status == 200) {
        events.push.apply(events, JSON.parse(httpObj.responseText));
        createEventsTable(events);

    }
}

function searchMoreEvents() {
    if (location != '') {
        makeXmlHttpRequest('widget/getEventsByLoc.php?location=' + eventsLocation + "&distance=" + eventsDistance + "&page=" + eventsPage, eventsStateHandler);
    }

}


function searchEvents() {
    eventsLocation = document.getElementById("locationBox").value;
    eventsDistance = document.getElementById("distanceBox").value;
    if (location != '') {
        makeXmlHttpRequest('widget/getEventsByLoc.php?location=' + eventsLocation + "&distance=" + eventsDistance + "&page=" + eventsPage, eventsStateHandler);
    }

}

function searchEventsMenu() {
    var div = getWidgetDiv();
    clearElement(div);

	// Location Box
	var locationBoxLabel = document.createElement("label");
    locationBoxLabel.setAttribute("for", "locationBox");
    locationBoxLabel.textContent = "Location";
	
    var locationBox = document.createElement("input")
    locationBox.id = "locationBox";
    locationBox.type = "text";
	div.appendChild(locationBoxLabel);
    div.appendChild(locationBox);
	
	div.appendChild(document.createElement("br"));

	// Distance Box
	var distanceBoxLabel = document.createElement("label");
    distanceBoxLabel.setAttribute("for", "distanceBox");
    distanceBoxLabel.textContent = "Distance (km)";
	var distanceBox = document.createElement("input")
    distanceBox.id = "distanceBox";
    distanceBox.type = "text";
	div.appendChild(distanceBoxLabel);
    div.appendChild(distanceBox);

    var searchButton = document.createElement("button");
    searchButton.textContent = "Search events";
    searchButton.onclick = searchEvents;
    div.appendChild(searchButton);
}

function initSearchMenu() {
    var div = getWidgetDiv(); // obter div da widget no documento
	if(div == null)
		return;

    // Caixa de texto para input do nome

    var artistBoxLabel = document.createElement("label");
    artistBoxLabel.setAttribute("for", "artistBox");
    artistBoxLabel.textContent = "Artist Name";
    div.appendChild(artistBoxLabel);

    var artistBox = document.createElement("input")
    artistBox.id = "artistBox";
    artistBox.type = "text";
    div.appendChild(artistBox);

    div.appendChild(document.createElement("br"));

    // Caixa de texto para input do numero de tags pretendida
    var numTagBoxLabel = document.createElement("label");
    numTagBoxLabel.setAttribute("for", "numTagBox");
    numTagBoxLabel.textContent = "Number of tags";
    div.appendChild(numTagBoxLabel);

    var numTagBox = document.createElement("input")
    numTagBox.id = "numTagBox";
    numTagBox.type = "text";
    div.appendChild(numTagBox);

    // Butao para submeter
    var searchButton = document.createElement("button");
    searchButton.textContent = "Get tags";
    searchButton.onclick = searchArtistTags;
    div.appendChild(searchButton);

    // Butao para submeter
    var searchEventsButton = document.createElement("button");
    searchEventsButton.textContent = "Look for events";
    searchEventsButton.onclick = searchEventsMenu;
    div.appendChild(searchEventsButton);
}

function documentReady() {

    /*
        Função para iniciar a 'execução' da widget quando acabar de carregar o documento onde ela se insere
    */
    if (document.readyState === "complete") { // documento acabou de carregar, inicializar widget
        initSearchMenu();
    }
    else {
        setTimeout(documentReady, 300); // documento não está pronto, invocar outra vez
    }
}




documentReady();