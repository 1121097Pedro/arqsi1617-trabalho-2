<?php
	include "lastFM.php";
	include "updateDB.php";
	
	// Verificar que há realmente um nome a procurar
	if(isset($_GET["name"]) && isset($_GET["numTags"])){
		$artistName = $_GET["name"];
		$NUMTAGS = $_GET["numTags"];
		
		// Caso o nome do artist tenha espaços, é necessário usar urlencode ou ocorre erro no pedido
		$url = "http://ws.audioscrobbler.com/2.0/?method=artist.gettoptags&artist=" . urlencode($artistName)."&api_key=$API_KEY";
		
		$respostaXML=file_get_contents($url);
		
		$newXML= new DOMDocument('1.0', 'ISO-8859-1');
		$newXML->loadXML($respostaXML);
		// navegar no XML com os métodos que já conhece, mas com uma sintaxe PHP para
		// aceder a objectos(->)
		$nodelist=$newXML->getElementsByTagName("name");
		$tags="";
		
		if($nodelist->length < $NUMTAGS){
			$NUMTAGS = $nodelist->length;
		}
		
		for ($i=0;$i<$NUMTAGS;$i++){
			$tagNode=$nodelist->item($i);
			$tagValue = $tagNode->nodeValue;
			$tags.=$tagValue;
			if($i+1 != $NUMTAGS){
				$tags .= ",";
			}

		}

		echo $tags;
	}
?>