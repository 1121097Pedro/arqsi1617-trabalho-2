<?php

session_start();
if (!isset($_SESSION["cart"])) {
    $_SESSION["cart"] = array();
}
$cart = (array) $_SESSION["cart"];
if (isset($_REQUEST["id"])) {
    $id = $_REQUEST["id"];
    if (isset($cart[$id])) {
        $cart[$id]++;
    } else {
        $cart[$id] = 1;
    }
}

$_SESSION["cart"] = $cart;
echo json_encode($cart);
?>