<?php
include 'WebServiceUtils.php';
include 'StockDAL.php';
$records = GetCatalog();

session_start();
if (isset($_POST["confirm"])) {
    $cart = (array) $_SESSION["cart"];
    $dal = CreateDAL();
    $dal->addOrder($cart, $records);
    $dal->close();
    MakeOrder($cart);
    unset($_SESSION["cart"]);
    header("Location: BrowseEditorCatalog.php");
    die();
} else {
    $cart = (array) $_SESSION["cart"];
    ?><html>
        <head>
            <meta http-equiv="Content-Type" content="text/html; charset=ISO 8859-1">
            <title>Music Shop</title>
            <link rel="stylesheet" type="text/css" href="css/admin.css"/>
            <script src="//ajax.googleapis.com/ajax/libs/jquery/1.8.2/jquery.min.js"></script>
            <link href="css/pag_principal.css" rel="stylesheet" type="text/css" media="all"/>
            <link href="css/catalog.css" rel="stylesheet" type="text/css" />
        </head>
        <body>
            <?php
            include 'header.php';
            ?>
            <div class="middle" id="middleDiv">
                <form method="POST">
                    Do you want to make the following order:
                    <table id="recordsTable" class="recordsTable">
                        <tr>
                            <th>Artist</th>
                            <th>Album</th>
                            <th>Format</th>
                            <th>Price</th>
                            <th>Quantity</th>
                        </tr>
                        <?php
                        $count = 0;
                        foreach ($cart as $RecordID => $qt) {
                            $count++;
                            if($count%2 == 0)
                                echo "<tr class='evenTR'>";
                            else 
                                echo "<tr class='oddTR'>";
                            echo "<td>" . $records[(string) $RecordID]['Artist'] . "</td>";
                            echo "<td>" . $records[(string) $RecordID]['Title'] . "</td>";
                            echo "<td>" . $records[(string) $RecordID]['Format'] . "</td>";
                            echo "<td>" . $records[(string) $RecordID]['Price'] . "</td>";
                            echo "<td>" . $qt . "</td>";
                            echo "</tr>";
                        }
                        ?>
                    </table>
                    <input type="hidden" name="confirm">
                    <input type='submit' value="Yes" />
                    <button type="button" class="buttonClass" onClick="javascript:location.href = 'BrowseEditorCatalog.php'">No</input>
                </form> 
            </div>
        </body>
    </html>
    <?php
}?>