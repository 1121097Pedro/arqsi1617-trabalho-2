    using IDEIMusic.DAL;
    using IDEIMusic.Models;
    using System;
    using System.Collections.Generic;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Linq;
    using System.Web.Security;
    using WebMatrix.WebData;

namespace IDEIMusic.Migrations
{

    internal sealed class Configuration : DbMigrationsConfiguration<IDEIMusic.DAL.EditorContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = false;
        }

        protected override void Seed(IDEIMusic.DAL.EditorContext context)
        {
            // Criar artistas
            var artists = new List<Artist> {
                new Artist { Name = "Pearl Jam"},
                new Artist { Name = "Depeche Mode"},
                new Artist { Name = "Eminem"},
                new Artist { Name = "System of a Down"},
                new Artist { Name = "David Guetta"}
            };

            artists.ForEach(s => context.Artists.AddOrUpdate(p => p.Name, s));
            context.SaveChanges();

            // Criar albums
            var albums = new List<Album>
            {
                new Album { Title ="Backspacer", ArtistID = artists.Single( a => a.Name == "Pearl Jam").ArtistID},
                new Album { Title = "Sounds of the Universe", ArtistID = artists.Single( a => a.Name == "Depeche Mode").ArtistID },
                new Album { Title = "Recovery", ArtistID = artists.Single( a => a.Name == "Eminem").ArtistID },
                new Album { Title = "Toxicity", ArtistID = artists.Single( a => a.Name == "System of a Down").ArtistID },
                new Album { Title = "Nothing but the beat", ArtistID = artists.Single( a => a.Name == "David Guetta").ArtistID },
            };

            albums.ForEach(a => context.Albums.AddOrUpdate(r => r.Title, a));
            context.SaveChanges();

            // Criar tags
            var tags = new List<Tag> {
                new Tag { Text = "alternative rock", Albums = new List<Album>()  },
                new Tag { Text = "rock", Albums = new List<Album>()  },
                new Tag { Text = "electronic", Albums = new List<Album>()  },
                new Tag { Text = "hip-hop", Albums = new List<Album>()  },
                new Tag { Text = "rap", Albums = new List<Album>()  },
                new Tag { Text = "alternative metal", Albums = new List<Album>()  },
                new Tag { Text = "dance", Albums = new List<Album>()  }
            };

            tags.ForEach(t => context.Tags.AddOrUpdate(r => r.Text, t));
            context.SaveChanges();

            var records = new List<Record>();
            records.Add(CreateRecord(context, 8.99m, RecordFormat.CD, "Eminem", "Recovery"));
            records.Add(CreateRecord(context, 6.99m, RecordFormat.CD, "System of a Down", "Toxicity"));
            records.Add(CreateRecord(context, 9.99m, RecordFormat.CD, "David Guetta", "Nothing but the beat"));
            records.Add(CreateRecord(context, 9.99m, RecordFormat.CD, "Pearl Jam", "Backspacer"));
            records.Add(CreateRecord(context, 16.99m, RecordFormat.Vinyl, "Depeche Mode", "Sounds of the Universe"));
            records.Add(CreateRecord(context, 17.99m, RecordFormat.Vinyl, "Pearl Jam", "Backspacer"));

            foreach (Record r in records)
            {
                Record result = context.Records.SingleOrDefault(record =>
                    record.AlbumID == r.AlbumID &&
                    record.Album.ArtistID == r.Album.ArtistID &&
                    record.RecordFormat == r.RecordFormat
                    );
                if (result == null)
                {
                    context.Records.Add(r);
                }
            }

            context.SaveChanges();

            // Adicionar tags a albums
            AddOrUpdateTag(context, "Pearl Jam", "Backspacer", "alternative rock");
            AddOrUpdateTag(context, "Pearl Jam", "Backspacer", "rock");
            AddOrUpdateTag(context, "Depeche Mode", "Sounds of the Universe", "electronic");
            AddOrUpdateTag(context, "Eminem", "Recovery", "hip-hop");
            AddOrUpdateTag(context, "Eminem", "Recovery", "rap");
            AddOrUpdateTag(context, "System of a Down", "Toxicity", "alternative metal");
            AddOrUpdateTag(context, "David Guetta", "Nothing but the beat", "electronic");
            AddOrUpdateTag(context, "David Guetta", "Nothing but the beat", "dance");

            SeedMembership();
            SeedSales(context);
        }

        private void SeedSales(EditorContext context)
        {
            var sales = new List<Sale>();
            Sale s = new Sale
            {
                 Quantity = 5,
                 Date = new DateTime(2014, 12, 1),
                 RecordID = context.Records.Single(
                 a => a.Album.Title == "Backspacer" 
                     && a.Album.Artist.Name == "Pearl Jam"
                     && a.RecordFormat == RecordFormat.Vinyl
                     ).RecordID
            };

            sales.Add(s);

            s = new Sale
            {
                 Quantity = 10,
                 Date = new DateTime(2014, 12, 1),
                 RecordID = context.Records.Single(
                 a => a.Album.Title == "Backspacer" 
                     && a.Album.Artist.Name == "Pearl Jam"
                     && a.RecordFormat == RecordFormat.CD
                     ).RecordID
            };
            sales.Add(s);

            s = new Sale
            {
                 Quantity = 5,
                 Date = new DateTime(2014, 12, 1),
                 RecordID = context.Records.Single(
                 a => a.Album.Title == "Sounds of the Universe" 
                     && a.Album.Artist.Name == "Depeche Mode"
                     && a.RecordFormat == RecordFormat.Vinyl
                     ).RecordID
            };
            sales.Add(s);

            s = new Sale
            {
                 Quantity = 4,
                 Date = new DateTime(2014, 12, 1),
                 RecordID = context.Records.Single(
                 a => a.Album.Title == "Nothing but the beat" 
                     && a.Album.Artist.Name == "David Guetta"
                     && a.RecordFormat == RecordFormat.CD
                     ).RecordID
            };
            sales.Add(s);

            s = new Sale
            {
                 Quantity = 9,
                 Date = new DateTime(2014, 12, 1),
                 RecordID = context.Records.Single(
                 a => a.Album.Title == "Toxicity" 
                     && a.Album.Artist.Name == "System of a Down"
                     && a.RecordFormat == RecordFormat.CD
                     ).RecordID
            };
            sales.Add(s);

            s = new Sale
            {
                 Quantity = 15,
                 Date = new DateTime(2014, 7, 1),
                 RecordID = context.Records.Single(
                 a => a.Album.Title == "Backspacer" 
                     && a.Album.Artist.Name == "Pearl Jam"
                     && a.RecordFormat == RecordFormat.CD
                     ).RecordID
            };
            sales.Add(s);

            foreach (Sale sale in sales)
            {
               context.Sales.Add(sale);
            }
            context.SaveChanges();

        }


        void AddOrUpdateTag(EditorContext context, string artistName, string albumTitle, string tagText)
        {
            var tag = context.Tags.SingleOrDefault(t => t.Text == tagText);
            var album = context.Albums.SingleOrDefault(a => a.Title == albumTitle && a.Artist.Name == artistName);
            if (!album.Tags.Contains(tag))
            {
                album.Tags.Add(tag);
            }
            if (!tag.Albums.Contains(album))
            {
                tag.Albums.Add(album);
            }
        }

        Record CreateRecord(EditorContext context, decimal price, RecordFormat format, string ArtistName, string AlbumTitle)
        {
            Record r = new Record
            {
                Price = price,
                RecordFormat = format,
                AlbumID = context.Albums.Single(a => a.Title == AlbumTitle && a.Artist.Name == ArtistName).AlbumID
            };

            r.Album = context.Albums.Find(r.AlbumID);
            return r;

        }
        private void SeedMembership()
        {
            WebSecurity.InitializeDatabaseConnection("DefaultConnection",
                "UserProfile", "UserId", "UserName", autoCreateTables: true);

            if (!Roles.RoleExists("Admin"))
            {
                Roles.CreateRole("Admin");
            }

            if (!Roles.RoleExists("ProductManager"))
            {
                Roles.CreateRole("ProductManager");
            }

            if (!Roles.RoleExists("ClientShop"))
            {
                Roles.CreateRole("ClientShop");
            }

            if (!WebSecurity.UserExists("Admin"))
            {
                WebSecurity.CreateUserAndAccount("Admin", "AdminAdmin", false);
                Roles.AddUserToRole("Admin", "Admin");
            }
            if (!WebSecurity.UserExists("ProductManager"))
            {
                WebSecurity.CreateUserAndAccount("ProductManager", "productmanager", false);
                Roles.AddUserToRole("ProductManager", "ProductManager");
            }
        }
    }
}
