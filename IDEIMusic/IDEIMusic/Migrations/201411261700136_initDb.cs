namespace IDEIMusic.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class initDb : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.UserProfile",
                c => new
                    {
                        UserId = c.Int(nullable: false, identity: true),
                        UserName = c.String(),
                    })
                .PrimaryKey(t => t.UserId);
            
            CreateTable(
                "dbo.Album",
                c => new
                    {
                        AlbumID = c.Int(nullable: false, identity: true),
                        ArtistID = c.Int(nullable: false),
                        Title = c.String(nullable: false),
                    })
                .PrimaryKey(t => t.AlbumID)
                .ForeignKey("dbo.Artist", t => t.ArtistID, cascadeDelete: true)
                .Index(t => t.ArtistID);
            
            CreateTable(
                "dbo.Tag",
                c => new
                    {
                        TagID = c.Int(nullable: false, identity: true),
                        Text = c.String(nullable: false),
                    })
                .PrimaryKey(t => t.TagID);
            
            CreateTable(
                "dbo.Artist",
                c => new
                    {
                        ArtistID = c.Int(nullable: false, identity: true),
                        Name = c.String(nullable: false),
                    })
                .PrimaryKey(t => t.ArtistID);
            
            CreateTable(
                "dbo.Record",
                c => new
                    {
                        RecordID = c.Int(nullable: false, identity: true),
                        AlbumID = c.Int(nullable: false),
                        RecordFormat = c.Int(nullable: false),
                        Price = c.Decimal(nullable: false, precision: 18, scale: 2),
                    })
                .PrimaryKey(t => t.RecordID)
                .ForeignKey("dbo.Album", t => t.AlbumID, cascadeDelete: true)
                .Index(t => t.AlbumID);
            
            CreateTable(
                "dbo.OrderDetail",
                c => new
                    {
                        OrderDetailID = c.Int(nullable: false, identity: true),
                        RecordID = c.Int(nullable: false),
                        Quantity = c.Int(nullable: false),
                        Subtotal = c.Decimal(nullable: false, precision: 18, scale: 2),
                    })
                .PrimaryKey(t => t.OrderDetailID)
                .ForeignKey("dbo.Record", t => t.RecordID, cascadeDelete: true)
                .Index(t => t.RecordID);
            
            CreateTable(
                "dbo.Order",
                c => new
                    {
                        OrderID = c.Int(nullable: false, identity: true),
                        OrderDate = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.OrderID);
            
            CreateTable(
                "dbo.ClientShop",
                c => new
                    {
                        UserID = c.Int(nullable: false),
                        Address = c.String(),
                        API_KEY = c.String(),
                        City = c.String(),
                        Country = c.String(),
                        Email = c.String(),
                        Name = c.String(),
                        PostalCode = c.String(),
                    })
                .PrimaryKey(t => t.UserID)
                .ForeignKey("dbo.UserProfile", t => t.UserID)
                .Index(t => t.UserID);
            
            CreateTable(
                "dbo.TagAlbum",
                c => new
                    {
                        Tag_TagID = c.Int(nullable: false),
                        Album_AlbumID = c.Int(nullable: false),
                    })
                .PrimaryKey(t => new { t.Tag_TagID, t.Album_AlbumID })
                .ForeignKey("dbo.Tag", t => t.Tag_TagID, cascadeDelete: true)
                .ForeignKey("dbo.Album", t => t.Album_AlbumID, cascadeDelete: true)
                .Index(t => t.Tag_TagID)
                .Index(t => t.Album_AlbumID);
            
        }
        
        public override void Down()
        {
            DropIndex("dbo.TagAlbum", new[] { "Album_AlbumID" });
            DropIndex("dbo.TagAlbum", new[] { "Tag_TagID" });
            DropIndex("dbo.ClientShop", new[] { "UserID" });
            DropIndex("dbo.OrderDetail", new[] { "RecordID" });
            DropIndex("dbo.Record", new[] { "AlbumID" });
            DropIndex("dbo.Album", new[] { "ArtistID" });
            DropForeignKey("dbo.TagAlbum", "Album_AlbumID", "dbo.Album");
            DropForeignKey("dbo.TagAlbum", "Tag_TagID", "dbo.Tag");
            DropForeignKey("dbo.ClientShop", "UserID", "dbo.UserProfile");
            DropForeignKey("dbo.OrderDetail", "RecordID", "dbo.Record");
            DropForeignKey("dbo.Record", "AlbumID", "dbo.Album");
            DropForeignKey("dbo.Album", "ArtistID", "dbo.Artist");
            DropTable("dbo.TagAlbum");
            DropTable("dbo.ClientShop");
            DropTable("dbo.Order");
            DropTable("dbo.OrderDetail");
            DropTable("dbo.Record");
            DropTable("dbo.Artist");
            DropTable("dbo.Tag");
            DropTable("dbo.Album");
            DropTable("dbo.UserProfile");
        }
    }
}
