namespace IDEIMusic.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class UpdatedSalesModel : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Sale",
                c => new
                    {
                        SaleID = c.Int(nullable: false, identity: true),
                        RecordID = c.Int(nullable: false),
                        Quantity = c.Int(nullable: false),
                        Date = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.SaleID)
                .ForeignKey("dbo.Record", t => t.RecordID, cascadeDelete: true)
                .Index(t => t.RecordID);
            
        }
        
        public override void Down()
        {
            DropIndex("dbo.Sale", new[] { "RecordID" });
            DropForeignKey("dbo.Sale", "RecordID", "dbo.Record");
            DropTable("dbo.Sale");
        }
    }
}
