namespace IDEIMusic.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class addAnnotationsToClientShop : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.ClientShop", "Address", c => c.String(nullable: false));
            AlterColumn("dbo.ClientShop", "City", c => c.String(nullable: false));
            AlterColumn("dbo.ClientShop", "Country", c => c.String(nullable: false));
            AlterColumn("dbo.ClientShop", "Email", c => c.String(nullable: false));
            AlterColumn("dbo.ClientShop", "Name", c => c.String(nullable: false));
            AlterColumn("dbo.ClientShop", "PostalCode", c => c.String(nullable: false));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.ClientShop", "PostalCode", c => c.String());
            AlterColumn("dbo.ClientShop", "Name", c => c.String());
            AlterColumn("dbo.ClientShop", "Email", c => c.String());
            AlterColumn("dbo.ClientShop", "Country", c => c.String());
            AlterColumn("dbo.ClientShop", "City", c => c.String());
            AlterColumn("dbo.ClientShop", "Address", c => c.String());
        }
    }
}
