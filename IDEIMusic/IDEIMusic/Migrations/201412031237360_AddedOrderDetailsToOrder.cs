namespace IDEIMusic.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddedOrderDetailsToOrder : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.OrderDetail", "Order_OrderID", c => c.Int());
            AddForeignKey("dbo.OrderDetail", "Order_OrderID", "dbo.Order", "OrderID");
            CreateIndex("dbo.OrderDetail", "Order_OrderID");
        }
        
        public override void Down()
        {
            DropIndex("dbo.OrderDetail", new[] { "Order_OrderID" });
            DropForeignKey("dbo.OrderDetail", "Order_OrderID", "dbo.Order");
            DropColumn("dbo.OrderDetail", "Order_OrderID");
        }
    }
}
