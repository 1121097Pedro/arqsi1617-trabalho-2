﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;

namespace IDEIMusic
{

    [DataContract]
    public class CatalogItem
    {
        [DataMember]
        public int RecordID { get; set; }
        [DataMember]
        public string Title { get; set; }
        [DataMember]
        public string Artist { get; set; }
        [DataMember]
        public string Format { get; set; }
        [DataMember]
        public decimal Price { get; set; }
        [DataMember]
        public ICollection<string> Tags { get; set; }

    }
    
    [ServiceContract]
    public interface IIDEIMusicService
    {
        [OperationContract]
        string GetKey(string loginName, string pw);

        [OperationContract]
        IList<CatalogItem> GetCatalog(string API_KEY);

        [OperationContract]
        int MakeOrder(string API_KEY, string[] OrderDetails);

        [OperationContract]
        int RegisterSale(string API_KEY, string SaleDetails);
    }
}
