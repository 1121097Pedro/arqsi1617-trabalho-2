﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace IDEIMusic.Models
{
    public class Artist
    {
        public int ArtistID { get; set; }

        [Required, Display(Name="Artist")]
        public string Name { get; set; }

        public virtual ICollection<Album> Albums { get; set; }
    }
}