﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace IDEIMusic.Models
{
    public class Tag
    {
        public int TagID { get; set; }

        [Required, Display(Name="Tag")]
        public string Text { get; set; }
        public virtual ICollection<Album> Albums { get; set; }
    }
}