﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace IDEIMusic.Models
{
    public class OrderDetail
    {
        public int OrderDetailID { get; set; }
        public int RecordID {get; set;}
        public int Quantity { get; set; }
        [DataType(DataType.Currency)]
        public decimal Subtotal { get; set; }
        public Record Record { get; set; }
       
    }
}