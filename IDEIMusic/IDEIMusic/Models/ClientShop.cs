﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace IDEIMusic.Models {
    public class ClientShop {
        [Key]
        [ForeignKey("UserProfile")]
        public int UserID { get; set; }

        public string Address { get; set; }
        [Display(Name = "API KEY")]
        public string API_KEY { get; set; }

        public string City { get; set; }

        public string Country { get; set; }

        public string Email { get; set; }

        [Display(Name = "Shop Name")]
        public string Name { get; set; }
        [Display(Name = "Postal Code")]
        public string PostalCode { get; set; }

        public virtual UserProfile UserProfile { get; set; }
    }
}