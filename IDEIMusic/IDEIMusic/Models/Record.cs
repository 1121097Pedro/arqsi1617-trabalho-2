﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace IDEIMusic.Models
{

    public enum RecordFormat
    {
        Vinyl, CD
    }

    public class Record
    {
        public int RecordID { get; set; }

        public int AlbumID { get; set; }

        [Display(Name = "Format")]
        public RecordFormat RecordFormat { get; set; }

        [DataType(DataType.Currency)]
        [Range(0.0,10000)]
        public decimal Price { get; set; }

        public virtual Album Album { get; set; }
    }
}