﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace IDEIMusic.Models
{
    public class Album
    {
        public int AlbumID { get; set; }
        public int ArtistID { get; set; }
        [Required]
        public string Title { get; set; }
        public virtual ICollection<Tag> Tags { get; set; }
        public virtual Artist Artist { get; set; }
    }
}