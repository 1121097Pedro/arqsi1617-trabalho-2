﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace IDEIMusic.Models
{
    public class Sale
    {
        public int SaleID { get; set; }
        public int RecordID { get; set; }

        public int Quantity { get; set; }

        [DisplayFormat(DataFormatString="{0:dd-MM-yyyy}", ApplyFormatInEditMode = true)]
        public DateTime Date { get; set; }

        public virtual Record Record { get; set; }
    }
}