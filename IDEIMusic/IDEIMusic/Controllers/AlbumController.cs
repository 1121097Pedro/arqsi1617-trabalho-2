﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using IDEIMusic.Models;
using IDEIMusic.DAL;
using IDEIMusic.ViewModels;

namespace IDEIMusic.Controllers {

    [Authorize(Roles = "ProductManager")]
    public class AlbumController : Controller {
        private EditorContext db = new EditorContext();

        //
        // GET: /Album/
        /*
         * Mostrar lista de albums em base de dados
         * */
        public ActionResult Index() {
            var albums = db.Albums.Include(a => a.Artist);
            return View(albums.ToList());
        }

        //
        // GET: /Album/Details/5
        /*
         * Mostrar detalhes de um album em base de dados
         * */

        public ActionResult Details(int id = 0) {
            Album album = db.Albums.Find(id);
            if (album == null) {
                return HttpNotFound();
            }
            return View(album);
        }

        //
        // GET: /Album/Create

        /*
         * Formulario para criação de um album
         * */
        public ActionResult Create() {
            Album album = new Album();
            album.Tags = new List<Tag>();
            PopulateAssignedTagsData(album); // Permite à view aceder a todas as tags
            return View();
        }

        //
        // POST: /Album/Create

        /*
         * Tratamento do formulario submetido para criação de album
         * */

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(FormCollection values, string[] selectedTags) {
            Album album = new Album();
            album.Tags = new List<Tag>();

            try {
                // Obter valores do formulario
                string artistName = values.GetValue("artist.Name").AttemptedValue;
                string albumTitle = values.GetValue("Title").AttemptedValue;

                album.Title = albumTitle;

                Artist artist = db.Artists
                    .FirstOrDefault(
                    a => a.Name.ToLower() == artistName.ToLower()); // comparação ignorando capitalização

                if (artist == null) // se o artista não existir, criar
                {
                    artist = new Artist();
                    artist.Name = artistName;
                }

                album.Artist = artist;

                UpdateAlbumTags(selectedTags, album); // obter quais foram as tags seleccionadas

                if (ModelState.IsValid) {
                    if(!albumExists(album)){
                        db.Albums.Add(album);
                        db.SaveChanges();
                        return RedirectToAction("Index");
                    }
                }

            }
            catch (DataException) {

            }

            PopulateAssignedTagsData(album);
            return View(album);
        }

        //
        // GET: /Album/Edit/5
        /*
         * Edição da informação de um album
         * */

        public ActionResult Edit(int id = 0) {
            Album album = db.Albums
                .Include(i => i.Tags)
                .Include(i => i.Artist)
                .Where(i => i.AlbumID == id)
                .Single();
            if (album == null) {
                return HttpNotFound();
            }

            PopulateAssignedTagsData(album); // Permite à view aceder às tags (escolhidas e não escolhidas)
            return View(album);
        }



        //
        // POST: /Album/Edit/5
        /*
         * Tratamento do formulario enviado para edição de um album
         * */
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(int id, FormCollection collection, string[] selectedTags) {
            Album album = db.Albums
                .Include(i => i.Tags)
                .Include(i => i.Artist)
                .Where(i => i.AlbumID == id)
                .Single();

            if (album == null) {
                return HttpNotFound();
            }

            if (TryUpdateModel(album, "", new string[] { "Title" })) {
                try {
                    UpdateAlbumTags(selectedTags, album);

                    db.Entry(album).State = EntityState.Modified;
                    db.SaveChanges();

                    return RedirectToAction("Index");
                }
                catch (DataException /* dex */) {

                }

            }

            return View(album);
        }



        // GET: /Album/Delete/5
        /*
         * Pedido de confirmação da remoção de um album
         * */
        public ActionResult Delete(int id = 0) {
            Album album = db.Albums.Find(id);
            if (album == null) {
                return HttpNotFound();
            }
            return View(album);
        }

        //
        // POST: /Album/Delete/5
        /*
         * Confirmação da remoção de um album
         * */

        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id) {
            Album album = db.Albums.Find(id);
            db.Albums.Remove(album);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        /*
         * Dado um album e uma lista de tags seleccionada, actualizar as tags do album
         * */
        private void UpdateAlbumTags(string[] selectedTags, Album album) {
            if (selectedTags == null) {
                album.Tags = new List<Tag>();
                return;
            }

            var selectedTagsHS = new HashSet<string>(selectedTags);
            var albumTags = new HashSet<int>(album.Tags.Select(t => t.TagID));

            foreach (var tag in db.Tags) {
                if (selectedTagsHS.Contains(tag.TagID.ToString())) {
                    if (!albumTags.Contains(tag.TagID)) {
                        album.Tags.Add(tag);
                    }
                }
                else {
                    if (albumTags.Contains(tag.TagID)) {
                        album.Tags.Remove(tag);
                    }
                }
            }
        }


        /*
         * Obter as tags todas e indicar quais seleccionadas
         * */
        private void PopulateAssignedTagsData(Album album) {
            var allTags = db.Tags;
            var albumTags = new HashSet<int>(album.Tags.Select(t => t.TagID));
            var viewModel = new List<AssignedTagsData>();
            foreach (var tag in allTags) {
                viewModel.Add(new AssignedTagsData {
                    TagID = tag.TagID,
                    Text = tag.Text,
                    Assigned = albumTags.Contains(tag.TagID)
                }
                    );
            }
            ViewBag.Tags = viewModel;
        }

        private bool albumExists(Album albumToCheck) {
            Album albumInDb = db.Albums.SingleOrDefault(s => 
                s.Title.ToLower() == albumToCheck.Title.ToLower() && 
                s.Artist.Name.ToLower() == albumToCheck.Artist.Name.ToLower());
            return albumInDb != null;
        }

        protected override void Dispose(bool disposing) {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}