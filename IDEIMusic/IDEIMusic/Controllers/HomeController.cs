﻿using IDEIMusic.DAL;
using IDEIMusic.Migrations;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace IDEIMusic.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            new Configuration();
            ViewBag.Message = "Welcome to IDEIMusic";

            return View();
        }

        public ActionResult About()
        {
            ViewBag.Message = "Editora IDEIMusic";

            return View();
        }

    }
}
