﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using IDEIMusic.Models;
using IDEIMusic.DAL;

namespace IDEIMusic.Controllers
{

    [Authorize(Roles="ProductManager")]
    public class RecordController : Controller
    {
        private EditorContext db = new EditorContext();

        //
        // GET: /Record/
        /*
         * Mostrar lista dos Records em base de dados
         * */
        public ActionResult Index()
        {
            var records = db.Records.Include(r => r.Album)
                .Include(r => r.Album.Artist)
                .Include(r => r.Album.Tags);
            return View(records.ToList());
        }

        //
        // GET: /Record/Details/5
        /*
         * Mostrar detalhes de um Record individual
         * */

        public ActionResult Details(int id = 0)
        {
            Record record = db.Records.Include(s => s.Album)
                .Include(s => s.Album.Artist)
                .Single( r => r.RecordID == id);
            if (record == null)
            {
                return HttpNotFound();
            }
            return View(record);
        }

        //
        // GET: /Record/Create
        /*
         * Formulario para criação de um Record
         * */

        public ActionResult Create()
        {
            ViewBag.ArtistID = new SelectList(db.Artists, "ArtistID", "Name");
            return View();
        }

        //
        // POST: /Record/Create
        /*
         * Tratamento de um formulário para criação de um Record
         * */

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "RecordFormat,Price")] Record record, FormCollection f)
        {
            string AlbumID = "";
            if(f.AllKeys.Contains("AlbumID")){
                AlbumID = f.GetValue("AlbumID").AttemptedValue;
            }
            
            if (AlbumID != "")
            {
                try {
                    int id = Convert.ToInt32(AlbumID);
                    record.AlbumID = Convert.ToInt32(AlbumID);
                    Record recordExists = db.Records.SingleOrDefault(
                        s => s.AlbumID == id
                        && s.RecordFormat == record.RecordFormat);
                    if (recordExists == null && record.Price >= 0.0m) {
                        if (ModelState.IsValid) {
                            db.Records.Add(record);
                            db.SaveChanges();
                            return RedirectToAction("Index");
                        }
                    }
                }
                catch (Exception) {
                    throw;
                }
            }
            ViewBag.ArtistID = new SelectList(db.Artists, "ArtistID", "Name");
            return View(record);
        }

        //
        // GET: /Record/Edit/5
        /*
         * Edição dos detalhes de um Record
         * */

        public ActionResult Edit(int id = 0)
        {
            Record record = db.Records.Include(s => s.Album)
                .Include(s => s.Album.Artist)
                .Single( r => r.RecordID == id);
            if (record == null)
            {
                return HttpNotFound();
            }

            return View(record);
        }

        //
        // POST: /Record/Edit/5
        /*
         * Tratamento do formulario recebido para edição dos detalhes de um Record
         * */
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(Record record)
        {
            if (ModelState.IsValid)
            {
                if (record.Price >= 0.0m) {
                    db.Entry(record).State = EntityState.Modified;
                    db.SaveChanges();
                    return RedirectToAction("Index");
                }
            }

            Record recordToDisplay = db.Records.Include(s => s.Album)
                .Include(s => s.Album.Artist)
                .Single( r => r.RecordID == record.RecordID);
            recordToDisplay.RecordFormat = record.RecordFormat;
            recordToDisplay.Price = record.Price;

            return View(recordToDisplay);
        }

        //
        // GET: /Record/Delete/5

        public ActionResult Delete(int id = 0)
        {
            Record record = db.Records.Include(s => s.Album)
                .Include(s => s.Album.Artist)
                .Single( r => r.RecordID == id);
            if (record == null)
            {
                return HttpNotFound();
            }
            return View(record);
        }

        //
        // POST: /Record/Delete/5

        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Record record = db.Records.Find(id);
            db.Records.Remove(record);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        /*
         * Retorna ids e titulos de um artista no formato JSON
         * */
        public ActionResult AjaxGetAlbumsFromArtist(int id)
        {
            var dictionary = db.Albums.Where(s => s.ArtistID == id).ToDictionary(a => a.AlbumID, a => a.Title);
            var ids = new List<int>();
            var titles = new List<string>();
            foreach(var k in dictionary){
                ids.Add(Convert.ToInt32(k.Key.ToString()));
                titles.Add(k.Value.ToString());
            }
            var idsTitles = new { ids, titles};
            return Json(idsTitles, JsonRequestBehavior.AllowGet);
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}