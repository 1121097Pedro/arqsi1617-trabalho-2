﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using IDEIMusic.Models;
using IDEIMusic.DAL;

namespace IDEIMusic.Controllers
{
    public class ArtistController : Controller
    {
        private EditorContext db = new EditorContext();

        // GET: /Artist/
        /*
         *  Listar Artists na base de dados
         * */
        public ActionResult Index()
        {
            return View(db.Artists.ToList());
        }

        //
        // GET: /Artist/Details/5
        /*
         *  Listar informação de um artista na base de dados
         * */

        public ActionResult Details(int id = 0)
        {
            Artist artist = db.Artists.Find(id);
            if (artist == null)
            {
                return HttpNotFound();
            }
            ViewBag.records = db.Records
                .Include(s=>s.Album)
                .Include(s=>s.Album.Artist)
                .Where(t => t.Album.ArtistID == id)
                .ToList();
            
            return View(artist);
        }

        //
        // GET: /Artist/Create
        /*
         *  Formulario para criar artista
         * */

        public ActionResult Create()
        {
            return View();
        }

        //
        // POST: /Artist/Create
        /*
         *  Tratamento do formulario submetido para a criação de um artista
         * */

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(Artist artist)
        {
            if (ModelState.IsValid)
            {
                if (!artistExists(artist)) { // Não pode haver artistas com nomes repetidos
                    db.Artists.Add(artist);
                    db.SaveChanges();
                    return RedirectToAction("Index");
                }
            }

            return View(artist);
        }

        //
        // GET: /Artist/Edit/5
        /*
         *  Formulario para editar um artista
         * */
        public ActionResult Edit(int id = 0)
        {
            Artist artist = db.Artists.Find(id);
            if (artist == null)
            {
                return HttpNotFound();
            }
            return View(artist);
        }

        //
        // POST: /Artist/Edit/5
        /*
         *  Tratamento do formulario submetido 
         *  para a edição da informação de  um artista
         * */
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(Artist artist)
        {
            if (ModelState.IsValid)
            {
                if (!artistExists(artist)) {// Não pode haver artistas com nomes repetidos
                    db.Entry(artist).State = EntityState.Modified;
                    db.SaveChanges();
                    return RedirectToAction("Index");
                }
            }
            return View(artist);
        }

        //
        // GET: /Artist/Delete/5
        /*
         * Página de confirmação na remoção de um artista da base de Dados
         * */
        public ActionResult Delete(int id = 0)
        {
            Artist artist = db.Artists.Find(id);
            if (artist == null)
            {
                return HttpNotFound();
            }

            ViewBag.records = db.Records
                .Include(s=>s.Album)
                .Include(s=>s.Album.Artist)
                .Where(t => t.Album.ArtistID == id)
                .ToList();
            return View(artist);
        }

        //
        // POST: /Artist/Delete/5
        /*
         * Confirmação de remoção de artista na base de dados
         * */
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Artist artist = db.Artists.Find(id);
            db.Artists.Remove(artist);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        /*
         * Devolve true se existir um artista em 
         * base de dados com o nome do artista submetido por parametro
         * */
        private bool artistExists(Artist a) {
            Artist artistExists = db.Artists.SingleOrDefault
                (s => s.Name.ToLower() == a.Name.ToLower());
            return artistExists != null;
        }
        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}