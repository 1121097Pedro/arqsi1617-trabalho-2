﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using IDEIMusic.Models;
using IDEIMusic.DAL;

namespace IDEIMusic.Controllers
{
    public class TagController : Controller
    {
        private EditorContext db = new EditorContext();

        //
        // GET: /Tag/
        /*
         * Mostrar lista de tags
         * */
        public ActionResult Index()
        {
            return View(db.Tags.ToList());
        }

        //
        // GET: /Tag/Details/5
        /*
         * Mostrar detalhes de uma tag
         * */
        public ActionResult Details(int id = 0)
        {
            Tag tag = db.Tags.Find(id);
            if (tag == null)
            {
                return HttpNotFound();
            }
            return View(tag);
        }

        //
        // GET: /Tag/Create
        /*
         * Formulario para criar tag
         * */
        public ActionResult Create()
        {
            return View();
        }

        //
        // POST: /Tag/Create
        /*
         * Tratamento de formulario submetido para criar uma tag
         * */
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(Tag tag)
        {
            if (ModelState.IsValid)
            {
                if (!tagExists(tag)) {
                    tag.Text = tag.Text.ToLower();
                    db.Tags.Add(tag);
                    db.SaveChanges();
                    return RedirectToAction("Index");
                }
            }

            return View(tag);
        }

        //
        // GET: /Tag/Edit/5
        /*
         * Formulario para editar uma tag especifica
         * */

        public ActionResult Edit(int id = 0)
        {
            Tag tag = db.Tags.Find(id);
            if (tag == null)
            {
                return HttpNotFound();
            }
            return View(tag);
        }

        //
        // POST: /Tag/Edit/5
        /*
         * Formulario submetido para editar uma tag
         * */

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(Tag tag)
        {
            if (ModelState.IsValid)
            {
                if (!tagExists(tag)) {
                    tag.Text = tag.Text.ToLower();
                    db.Entry(tag).State = EntityState.Modified;
                    db.SaveChanges();
                    return RedirectToAction("Index");
                }
            }
            return View(tag);
        }

        //
        // GET: /Tag/Delete/5
        /*
         * Formulario para confirmação da remoção de uma tag
         * */
        public ActionResult Delete(int id = 0)
        {
            Tag tag = db.Tags.Find(id);
            if (tag == null)
            {
                return HttpNotFound();
            }
            return View(tag);
        }

        //
        // POST: /Tag/Delete/5
        /*
         * Confirmação da remoção
         * */
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Tag tag = db.Tags.Find(id);
            db.Tags.Remove(tag);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }

        protected bool tagExists(Tag t) {
            Tag tagInDb = db.Tags.SingleOrDefault(a => a.Text.ToLower() == t.Text.ToLower());
            return tagInDb != null;
        }
    }
}