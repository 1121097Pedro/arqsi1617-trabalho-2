﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using IDEIMusic.Models;
using IDEIMusic.DAL;
using WebMatrix.WebData;
using IDEIMusic.Util;

namespace IDEIMusic.Controllers
{
    public class ClientShopController : Controller
    {
        private EditorContext db = new EditorContext();

        //
        // GET: /ClientShop/

        [Authorize(Roles="Admin")]
        public ActionResult Index()
        {
            var clientshops = db.ClientShops.Include(c => c.UserProfile);
            return View(clientshops.ToList());
        }

        //
        // GET: /ClientShop/Details/5

        [Authorize(Roles = "ClientShop")]
        public ActionResult SetInformation()
        {
            int userID = WebSecurity.GetUserId(User.Identity.Name);
            ClientShop clientshop = db.ClientShops.Find(userID);
            if (clientshop == null)
            {
                return HttpNotFound();
            }
            return View(clientshop);
        }


        //
        // POST: /ClientShop/EditDetails
        [Authorize(Roles = "ClientShop")]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult SetInformation([Bind(Include = "UserID,Name,Address,City,Country,Email,PostalCode")] ClientShop clientshop)
        {
            // Por uma questão de segurança queremos confirmar que a pessoa a editar é a pessoa autenticada
            if (clientshop.UserID != WebSecurity.GetUserId(User.Identity.Name))
            {
                return HttpNotFound();
            }
            if (ModelState.IsValid)
            {
                db.Entry(clientshop).State = EntityState.Modified;
                db.SaveChanges();
                if (string.IsNullOrEmpty(clientshop.API_KEY))
                {
                    clientshop.API_KEY = GUIDHandler.generateAPI_KEY();
                    db.Entry(clientshop).State = EntityState.Modified;
                    db.SaveChanges();
                }
                return RedirectToAction("InformationValidated");
            }
            ViewBag.UserID = new SelectList(db.UserProfiles, "UserId", "UserName", clientshop.UserID);
            return View(clientshop);
        }

        [Authorize(Roles = "ClientShop")]
        public ActionResult InformationValidated()
        {

            int userID = WebSecurity.GetUserId(User.Identity.Name);
            ClientShop clientshop = db.ClientShops.Find(userID);
            if (clientshop == null)
            {
                return HttpNotFound();
            }
            else
            {
                if (!validShop(clientshop))
                    return RedirectToAction("SetInformation", "ClientShop");
            }

            if (string.IsNullOrEmpty(clientshop.API_KEY))
            {
                clientshop.API_KEY = GUIDHandler.generateAPI_KEY();
                db.Entry(clientshop).State = EntityState.Modified;
                db.SaveChanges();
            }

            return View(clientshop);
        }

        [Authorize(Roles = "ClientShop")]
        public ActionResult MyShop()
        {
            int userID = WebSecurity.GetUserId(User.Identity.Name);
            ClientShop clientshop = db.ClientShops.Find(userID);
            if (clientshop == null)
            {
                return HttpNotFound();
            }
            if (!validShop(clientshop))
            {
                return RedirectToAction("SetInformation");
            }

            return View(clientshop);
        }

        private Boolean validShop(ClientShop c)
        {
            if (string.IsNullOrEmpty(c.Address) ||
                string.IsNullOrEmpty(c.City) ||
                string.IsNullOrEmpty(c.Country) ||
                string.IsNullOrEmpty(c.Email) ||
                string.IsNullOrEmpty(c.Name) ||
                string.IsNullOrEmpty(c.PostalCode)
                )
            {
                return false;
            }

            return true;
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}