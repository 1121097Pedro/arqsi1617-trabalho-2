﻿using IDEIMusic.DAL;
using IDEIMusic.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace IDEIMusic.Controllers {
    [Authorize(Roles = "Admin")]
    public class SalesController : Controller {
        EditorContext db = new EditorContext();
        //
        // GET: /Sales/

        [Authorize(Roles = "Admin")]
        public ActionResult Index(int year = -1, int month = -1) {
            DateTime now = DateTime.Now;
            int salesYear, salesMonth;
            if (year == -1 || month == -1) {
                salesYear = now.Year;
                salesMonth = now.Month;
            }
            else {
                salesYear = year;
                salesMonth = month;
            }
            if (now.Year < salesYear )
                salesYear = now.Year;
            if (salesMonth < 1 || salesMonth > 12)
                salesMonth = 1;
            ViewBag.year = salesYear;
            ViewBag.monthName = new DateTime(salesYear, salesMonth, 1).ToString("MMMM", CultureInfo.InvariantCulture);
            CalculateSalesByFormat(salesYear, salesMonth);
            CalculateSalesByTags(salesYear, salesMonth);
            CalculateSalesByArtist(salesYear, salesMonth);
            return View();
        }
        private void CalculateSalesByArtist(int year, int month) {
            var artistSalesKVPair = new Dictionary<string, int>();
            var sales = db.Sales.Where(
                                 s => s.Date.Year == year
                                 && s.Date.Month == month).ToList();
            foreach (Sale s in sales) {
                if (artistSalesKVPair.ContainsKey(s.Record.Album.Artist.Name)) {
                    artistSalesKVPair[s.Record.Album.Artist.Name] += s.Quantity;
                }
                else {
                    artistSalesKVPair[s.Record.Album.Artist.Name] = s.Quantity;
                }
            }

            ViewBag.SalesByArtist = artistSalesKVPair;

        }

        private void CalculateSalesByTags(int year, int month) {
            var keyValuePairs = new Dictionary<string, int>();
            List<Sale> sales = db.Sales.Where(
                                    s => s.Date.Year == year
                                    && s.Date.Month == month).ToList();
            foreach (Sale s in sales) {
                foreach (Tag t in s.Record.Album.Tags) {
                    if (keyValuePairs.ContainsKey(t.Text)) {
                        keyValuePairs[t.Text] += s.Quantity;
                    }
                    else {
                        keyValuePairs[t.Text] = s.Quantity;
                    }
                }
            }
            ViewBag.SalesByTags = keyValuePairs;

        }

        private void CalculateSalesByFormat(int year, int month) {
            var vinylSales = db.Sales.Where(
                                    s => s.Date.Year == year
                                    && s.Date.Month == month
                                    && s.Record.RecordFormat == Models.RecordFormat.Vinyl);
            if (vinylSales.Count() > 0) {
                ViewBag.VinylSales = vinylSales.Select(s => s.Quantity).Sum();
            }
            else {
                ViewBag.VinylSales = 0;
            }
            var cdSales = db.Sales.Where(
                                s => s.Date.Year == year
                                && s.Date.Month == month
                                && s.Record.RecordFormat == Models.RecordFormat.CD);
            if (cdSales.Count() > 0) {
                ViewBag.CDSales = cdSales.Select(s => s.Quantity).Sum();
            }
            else {
                ViewBag.CDSales = 0;
            }
        }

    }
}
