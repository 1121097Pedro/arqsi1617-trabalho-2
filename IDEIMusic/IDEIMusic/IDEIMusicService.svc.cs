﻿using IDEIMusic.DAL;
using IDEIMusic.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using WebMatrix.WebData;

namespace IDEIMusic {
    public class IDEIMusicService : IIDEIMusicService {
        EditorContext db = new EditorContext();

        public string GetKey(string loginName, string pw) {
            string api_key = "";
            if (WebSecurity.Login(loginName, pw, false)) {
                ClientShop c = db.ClientShops.SingleOrDefault(s => s.UserProfile.UserName.ToLower() == loginName.ToLower());
                if (c != null)
                    api_key = c.API_KEY;
            }

            return api_key;
        }
        public IList<CatalogItem> GetCatalog(string API_KEY) {
            List<Record> recordList = db.Records.ToList();
            List<CatalogItem> catalog = new List<CatalogItem>();
            ClientShop u = GetUser(API_KEY);
            if (u == null) {
                return catalog;
            }

            foreach (Record r in recordList) {
                catalog.Add(new CatalogItem {
                    Artist = r.Album.Artist.Name,
                    Title = r.Album.Title,
                    Price = r.Price,
                    Format = r.RecordFormat.ToString(),
                    RecordID = r.RecordID,
                    Tags = r.Album.Tags.Select(s => s.Text).ToList()
                });
            }

            return catalog;
        }

        public int MakeOrder(string API_KEY, string[] OrderDetails) {
            int count = 0;
            OrderDetail subOrder;
            ClientShop c = GetUser(API_KEY);
            if (c == null) {
                return -1;
            }

            Order order = new Order {
                OrderDate = DateTime.Now,
                OrderDetails = new List<OrderDetail>()
            };
            foreach (string s in OrderDetails) {
                string[] orderDetail = s.Split(',');
                if (orderDetail.Length != 2) {
                    continue;
                }
                else {
                    subOrder = CreateOrderDetail(orderDetail[0], orderDetail[1]);
                    if (subOrder != null) {
                        order.OrderDetails.Add(subOrder);
                        count++;
                    }
                }
            }
            if (order.OrderDetails.Count > 0) {
                db.Order.Add(order);
                db.SaveChanges();
            }
            return count;
        }

        public int RegisterSale(string API_KEY, string SaleDetails) {
            string[] saleDetails = SaleDetails.Split(';');
            ClientShop c = GetUser(API_KEY);
            if (c == null) {
                return -1;
            }
            Record r = TryGetRecord(saleDetails[0],saleDetails[1],saleDetails[2]);
            if (r != null) {
                try {
                    int quantity = Convert.ToInt32(saleDetails[3]);
                    Sale s = CreateSale(r, quantity);
                    db.Sales.Add(s);
                    db.SaveChanges();
                    return 0;
                }
                catch(Exception){
                }
            }
            return 1;
        }

        private Sale CreateSale(Record r, int quantity) {
            Sale s = new Sale {
                Date = DateTime.Now,
                Quantity = quantity,
                RecordID = r.RecordID
            };
            return s;
        }


        private OrderDetail CreateOrderDetail(string RecordID, string Quantity) {
            Record r = GetRecord(RecordID);
            OrderDetail s = null;
            try {
                int qty = Convert.ToInt32(Quantity);
                if (r != null) {
                    s = new OrderDetail {
                        RecordID = r.RecordID,
                        Quantity = qty,
                        Subtotal = qty * r.Price
                    };
                }
            }
            catch (Exception ex) {

            }
            return s;
        }
        private ClientShop GetUser(string API_KEY) {
            if (String.IsNullOrEmpty(API_KEY)) {
                return null;
            }
            ClientShop c = db.ClientShops.SingleOrDefault(s => s.API_KEY == API_KEY);
            return c;
        }

        private Record GetRecord(string RecordID) {
            int id;
            Record r = null;
            try {
                id = Convert.ToInt32(RecordID);
                r = db.Records.SingleOrDefault(s => s.RecordID == id);
            }
            catch (Exception e) {
                if (e is FormatException || e is OverflowException) {

                }
                else {

                }
            }
            return r;
        }

        private Record TryGetRecord(string Artist, string Title, string Format) {
            Record r = null;
            RecordFormat f;
            if (Format == "Vinyl") {
                f = RecordFormat.Vinyl;
            }
            else {
                f = RecordFormat.CD;
            }
            try {
                r = db.Records.SingleOrDefault(
                    s => s.Album.Title.ToLower() == Title.ToLower()
                        && s.RecordFormat == f
                        && s.Album.Artist.Name.ToLower() == Artist);
            }
            catch (Exception) {

            }
            return r;
        }
    }
}
