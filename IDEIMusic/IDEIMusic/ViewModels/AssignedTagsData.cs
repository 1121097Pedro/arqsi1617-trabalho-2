﻿using IDEIMusic.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace IDEIMusic.ViewModels
{
    public class AssignedTagsData
    {
        public int TagID { get; set; }

        public string Text { get; set; }
        public bool Assigned { get; set; }
    }
}
